# UXF CMS

## Install

```
$ composer require uxf/cms
```

## Config

```php
// config/routes/uxf.php
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return static function (RoutingConfigurator $routes): void {
    $routes->import('@UXFCmsBundle/config/routes.php');
    $routes->import('@UXFSecurityBundle/config/routes.php');
    $routes->import('@UXFContentBundle/config/routes.php');
    $routes->import('@UXFDataGridBundle/config/routes.php');
    $routes->import('@UXFFormBundle/config/routes.php');
};
```

```php
// config/packages/uxf.php
use UXF\CMS\Entity\User;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->extension('uxf_cms', [
        'audit_token' => 'xxx', # optional
        'mailing' => [
            'email_from' => 'info@uxf.cz',
            'email_name' => 'UXF info',
            'invitation_url' => 'https://uxf.cz/renew-password/',
            'recovery_url' => 'https://uxf.cz/renew-password/',
            'cms_recovery_url' => 'https://uxf.cz/admin/cms/renew-password/',
            'tag' => env('PROJECT_TAG'),
            'email_previews' => '%kernel.project_dir%/src/EmailZone/Email',
        ],
        'storage' => [
            'default_upload_namespace' => 'cms',
        ],
        'cron' => [
            'stage' => 'wow',                         # optional (default value from logger.sentry.environment)
            'sources' => [__DIR__ . '/../Project'],   # optional (default ./src)
            'destinations' => [__DIR__ . '/cron'],    # optional (default ./docker/cron)
            'enabled_stages' => ['magic'],            # optional (default [prod, dev])
        ],
        'loggable' => [
            'sources' => '%kernel.project_dir%/src/Entity',
        ],
        'logger' => [
            'stdout' => [
                'enabled' => true, # default false
                'level' => 'notice', # optional
            ],
            'sentry' => [
                'dsn' => '%env(SENTRY_DSN)%',
                'environment' => '%env(SENTRY_ENV)%',
                'release' => 'none', # optional
                'level' => 'warning', # optional
                'traces_sample_rate' => 0.5, # default 0
            ],
        ],
    ]);
};
```

## CronJob

- Send monitoring info to sentry
- Generate crontab file
- Enable run commands only on enabled stages

```bash
$ bin/console uxf:cron-gen
```

```php
use Symfony\Component\Console\Attribute\AsCommand;
use UXF\CMS\Attribute\CronJob;

#[CronJob(crontab: '*/5 * * * *', slug: 'Ring', args: '--wizard', enabledStages: ['prod'])]
#[AsCommand('app:cron:magic')]
final class MagicCronCommand extends Command
{
    ...
}
```

## Autocomplete

### A) CmsMeta attribute

Add `autocompleteFields` to `CmsMeta`. New autocomplete is registered automatically with name by alias.

```php
#[ORM\Entity]
#[ORM\Table(name: 'users', schema: 'uxf_cms')]
#[CmsMeta(alias: 'user', title: 'User', autocompleteFields: ['name', 'email'])]
class User
{
    ...
}
```

### B) CmsForm/CmsGrid attribute (override default autocomplete)

```php
#[CmsForm(label: 'Uživatelská role', targetField: 'title', autocomplete: 'role')]
#[CmsGrid(label: 'Uživatelská role', targetColumn: 'title', autocomplete: 'role')]
#[ORM\ManyToOne]
private Role $role;
```

### C) AutocompleteType

```php

final class CategoryType implements AutocompleteType
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * @param mixed[] $options
     */
    public function buildAutocomplete(AutocompleteBuilder $builder, array $options = []): void
    {
        // you can use 4th $labelQuery argument - eg. '{name} {surname} ({email})'
        $autocomplete = DoctrineAutocomplete::create($this->entityManager, Category::class, ['name']);

        // you can modify QB conditions
        $autocomplete->qb->andWhere('e.id > 1');

        $builder->setAutocomplete($autocomplete);
    }

    public static function getName(): string
    {
        return 'content-category';
    }
}
```

### D) AutocompleteInterface - total custom

```php
class TestAutocomplete implements Autocomplete
{
    /**
     * @return AutocompleteResult[]
     */
    public function search(string $term, int $limit): array
    {
        ...
    }
}
```

## Security

```php
use Symfony\Component\Routing\Attribute\Route;
use UXF\Core\Attribute\Security;

final readonly class TestController
{
    #[Route(path: '/api/test', name: 'test', methods: 'GET')]
    #[Security(UserRole::ROLE_PROFILE)] // use 'PUBLIC' for anonymous access, 'LOGGED' for logged user (role is ignored)
    public function __invoke(AppRequest $appRequest): mixed
    {
        ...
    }
}
```

## Email previews

All email previews configured in source directory `uxf_cms.mailing.email_previews` are listed at `/api/cms/email/list` 

```php
final class SampleEmail extends PreviewableEmail
{
    public function __construct(
        public readonly string $url,
    ) {
        parent::__construct('/email-templates.twig');
    }

    /**
     * @return self[]
     */
    public static function getPreviewData(): array
    {
        return [
            new self('https://uxf.cz'),
        ];
    }
}
```

### send email preview command

To test email clients there is command:
```shell
bin/console uxf:email:send-preview your-email@uxf.cz app_email-zone_email_organization-application-accepted-email
```

email identifier is same as preview url

## Loggable

configuration 
```
// config/packages/uxf.php
use UXF\CMS\Entity\User;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $containerConfigurator->extension('uxf_cms', [
        ...
        'loggable' => [
            'email_previews' => [
                '%kernel.project_dir%/src/**/Entity'
            ],
        ]
    ]);
};

```