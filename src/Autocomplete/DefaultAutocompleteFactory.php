<?php

declare(strict_types=1);

namespace UXF\CMS\Autocomplete;

use LogicException;
use Psr\Container\ContainerInterface;
use UXF\Core\Contract\Autocomplete\Autocomplete;
use UXF\Core\Contract\Autocomplete\AutocompleteFactory;

final readonly class DefaultAutocompleteFactory implements AutocompleteFactory
{
    public function __construct(
        private AutocompleteBuilderFactory $autocompleteBuilderFactory,
        private ContainerInterface $container,
    ) {
    }

    /**
     * @param mixed[] $options
     */
    public function createAutocomplete(string $name, array $options = []): Autocomplete
    {
        $builder = $this->autocompleteBuilderFactory->tryCreateBuilder($name) ?? new AutocompleteBuilder();

        $this->findAutocompleteType($name)?->buildAutocomplete($builder, $options);

        return $builder->build();
    }

    private function findAutocompleteType(string $name): ?AutocompleteType
    {
        $name = "autocomplete.$name";

        if ($this->container->has($name)) {
            $autocompleteType = $this->container->get($name);
            if (!$autocompleteType instanceof AutocompleteType) {
                throw new LogicException("Invalid type $name");
            }

            return $autocompleteType;
        }

        return null;
    }
}
