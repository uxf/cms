<?php

declare(strict_types=1);

namespace UXF\CMS\Autocomplete\Adapter;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Nette\Utils\Strings;
use UXF\Core\Contract\Autocomplete\Autocomplete;
use UXF\Core\Contract\Autocomplete\AutocompleteResult;

final readonly class DoctrineAutocomplete implements Autocomplete
{
    /**
     * Example QB definition:
     *
     * $qb->select('NEW UXF\Core\Contract\Autocomplete\AutocompleteResult(e.id, e.label)')
     *    ->andWhere('e.label LIKE :term');
     */
    public function __construct(
        public QueryBuilder $qb,
    ) {
    }

    /**
     * @param class-string<object> & literal-string $className
     * @param literal-string[] $fields
     * @param literal-string|null $labelQuery
     */
    public static function create(EntityManagerInterface $entityManager, string $className, array $fields, ?string $labelQuery = null): self
    {
        $filterFields = implode(", ' ',", array_map(static fn (string $field) => "COALESCE(CAST(e.$field AS text), '')", $fields));
        $selectFields = $labelQuery !== null ? self::generateQueryString($labelQuery, 'e') : $filterFields;

        $qb = $entityManager->createQueryBuilder()
            ->from($className, 'e')
            ->select('NEW ' . AutocompleteResult::class . "(e.id, CONCAT($selectFields, ''))")
            ->andWhere("UNACCENT(CONCAT($filterFields, '')) LIKE :term");

        foreach ($fields as $field) {
            $qb->addOrderBy("e.$field", 'ASC');
        }

        return new self($qb);
    }

    /**
     * @return AutocompleteResult[]
     */
    public function search(string $term, int $limit): array
    {
        $term = Strings::lower(Strings::toAscii($term));

        return $this->qb
            ->setParameter('term', "%$term%")
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param literal-string $string
     * @param literal-string $rootAlias
     * @return literal-string
     */
    public static function generateQueryString(string $string, string $rootAlias): string
    {
        $words = Strings::split($string, '/({[a-z]+})/i', PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

        /** @var literal-string $result */
        $result = implode(', ', array_map(
            static fn (string $w) => $w[0] === '{'
                ? "COALESCE(CAST($rootAlias." . trim($w, '{}') . " AS text), '')"
                : "'$w'",
            $words,
        ));

        return $result;
    }
}
