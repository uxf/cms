<?php

declare(strict_types=1);

namespace UXF\CMS\GQL\Mutation;

use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Mutation as LegacyMutation;
use UXF\CMS\Security\LoggedUserProvider;
use UXF\CMS\Service\UserConfigService;
use UXF\GraphQL\Attribute\Mutation;
use UXF\GraphQL\Type\Json;

final readonly class UserConfigMutation
{
    public function __construct(
        private UserConfigService $userConfigService,
        private LoggedUserProvider $loggedUserProvider,
    ) {
    }

    #[Logged]
    #[LegacyMutation(name: 'userConfig')]
    #[Mutation('userConfig')]
    public function __invoke(string $name, Json $data): Json
    {
        return new Json($this->userConfigService->save($name, $data, $this->loggedUserProvider->getUser())->getData());
    }
}
