<?php

declare(strict_types=1);

namespace UXF\CMS\GQL\Type;

use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type as LegacyType;
use UXF\GraphQL\Attribute\Type;

#[LegacyType]
#[Type]
final readonly class Autocomplete
{
    public function __construct(
        #[Field] public string $id,
        #[Field] public string $label,
    ) {
    }
}
