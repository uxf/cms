<?php

declare(strict_types=1);

namespace UXF\CMS\GQL;

use UXF\CMS\GQL\Type\Autocomplete;
use UXF\Core\Contract\Autocomplete\AutocompleteFactory;
use UXF\Core\Contract\Autocomplete\AutocompleteResult;

final readonly class AutocompleteResponseProvider
{
    public function __construct(
        private AutocompleteFactory $autocompleteFactory,
    ) {
    }

    /**
     * @param mixed[] $options
     * @return Autocomplete[]
     */
    public function get(string $name, string $term, int $limit, array $options = []): array
    {
        $results = $this->autocompleteFactory->createAutocomplete($name, $options)->search(trim($term), $limit);
        return array_map(static fn (AutocompleteResult $item) => new Autocomplete((string) $item->id, $item->label), $results);
    }
}
