<?php

declare(strict_types=1);

namespace UXF\CMS\Mime;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;

abstract class PreviewableEmail extends TemplatedEmail
{
    public function __construct(string $htmlTemplate)
    {
        parent::__construct();
        $this->htmlTemplate($htmlTemplate);
        $this->context((array) $this);
    }

    /**
     * @return iterable<string, self>
     */
    abstract public static function getPreviewData(): iterable;
}
