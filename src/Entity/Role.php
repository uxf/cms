<?php

declare(strict_types=1);

namespace UXF\CMS\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use UXF\CMS\Attribute\CmsForm;
use UXF\CMS\Attribute\CmsGrid;
use UXF\CMS\Attribute\CmsMeta;
use UXF\Core\Attribute\AutocompleteFields;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\Type\DateTime;

#[ORM\Entity]
#[ORM\Table(schema: 'uxf_cms')]
#[ORM\HasLifecycleCallbacks]
#[CmsMeta(alias: 'role', title: 'Role')]
#[AutocompleteFields(name: 'role', fields: ['title'])]
class Role implements JsonSerializable
{
    public const string ROLE_ROOT = 'ROLE_ROOT';
    public const string ROLE_ADMIN = 'ROLE_ADMIN';
    public const string ROLE_USER = 'ROLE_USER';

    #[ORM\Column, ORM\Id, ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[CmsGrid(hidden: true)]
    #[CmsForm(label: 'Id', readOnly: true)]
    private ?int $id = null;

    #[ORM\Column(length: 45, unique: true)]
    #[CmsGrid(label: 'Název role')]
    #[CmsForm(label: 'Název role', readOnly: true)]
    private string $name = '';

    #[ORM\Column(length: 45)]
    #[CmsGrid(label: 'Label role')]
    #[CmsForm(label: 'Label role', editable: true)]
    private string $title = '';

    #[ORM\Column(type: DateTime::class)]
    #[CmsGrid(label: 'Vytvořeno')]
    #[CmsForm(label: 'Vytvořeno', readOnly: true)]
    private DateTime $createdAt;

    #[ORM\Column(type: DateTime::class, nullable: true)]
    #[CmsGrid(label: 'Čas poslední úpravy')]
    #[CmsForm(label: 'Čas poslední úpravy', readOnly: true)]
    private ?DateTime $updatedAt = null;

    public function __construct()
    {
        $this->createdAt = Clock::now();
    }

    #[ORM\PreUpdate]
    public function onPreUpdate(): void
    {
        $this->updatedAt = Clock::now();
    }

    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed[]
     */
    public function jsonSerialize(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "title" => $this->title,
            "createdAt" => $this->createdAt,
            "updatedAt" => $this->updatedAt,
        ];
    }
}
