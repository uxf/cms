<?php

declare(strict_types=1);

namespace UXF\CMS\Entity;

use Doctrine\ORM\Mapping as ORM;
use UXF\CMS\Attribute\CmsForm;
use UXF\CMS\Attribute\CmsGrid;

#[ORM\Entity]
#[ORM\Table(name: 'user_config', schema: 'uxf_cms')]
#[ORM\UniqueConstraint(name: 'user_config_name_user', columns: ['name', 'user_id'])]
class UserConfig
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[CmsGrid(hidden: true)]
    #[CmsForm(label: 'Id', readOnly: true)]
    private ?int $id = 0;

    #[ORM\Column]
    private string $name;

    #[ORM\ManyToOne, ORM\JoinColumn(nullable: false)]
    private User $user;

    #[ORM\Column(type: 'json')]
    private mixed $data;

    public function __construct(User $user, string $name, mixed $data)
    {
        $this->user = $user;
        $this->name = $name;
        $this->data = $data;
    }

    public function getId(): int
    {
        return (int) $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getData(): mixed
    {
        return $this->data;
    }

    public function setData(mixed $data): self
    {
        $this->data = $data;
        return $this;
    }
}
