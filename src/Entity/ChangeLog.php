<?php

declare(strict_types=1);

namespace UXF\CMS\Entity;

use Doctrine\ORM\Mapping as ORM;
use UXF\CMS\Attribute\CmsGrid;
use UXF\CMS\Attribute\CmsMeta;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\Type\DateTime;

#[ORM\Entity]
#[ORM\Table(schema: 'uxf_cms')]
#[ORM\Index(name: 'changelog_index', fields: ['entityName', 'entityKey'])]
#[CmsMeta(alias: 'change-log', title: 'Žurnál změn')]
class ChangeLog
{
    #[ORM\Id]
    #[ORM\Column]
    #[CmsGrid(label: 'Id')]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id = 0;

    #[ORM\ManyToOne]
    #[CmsGrid(label: 'Uživatel', targetColumn: 'email')]
    private ?User $user;

    #[CmsGrid(label: 'Datum', type: 'datetime')]
    #[ORM\Column(type: DateTime::class)]
    private DateTime $createdAt;

    #[ORM\Column]
    #[CmsGrid(label: 'Název entity')]
    private string $entityName;

    #[ORM\Column]
    #[CmsGrid(label: 'Id entity')]
    private string $entityKey;

    /** @var array<string, mixed> */
    #[CmsGrid(label: 'Hodnoty')]
    #[ORM\Column(name: '`values`', type: 'json', options: [
        'jsonb' => true,
    ])]
    private array $values;

    /**
     * @param array<string, mixed> $values
     */
    public function __construct(?User $user, string $entityName, string $entityKey, array $values)
    {
        $this->user = $user;
        $this->createdAt = Clock::now();
        $this->entityName = $entityName;
        $this->entityKey = $entityKey;
        $this->values = $values;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getEntityName(): string
    {
        return $this->entityName;
    }

    public function getEntityKey(): string
    {
        return $this->entityKey;
    }

    /**
     * @return array<string, mixed>
     */
    public function getValues(): array
    {
        return $this->values;
    }
}
