<?php

declare(strict_types=1);

namespace UXF\CMS\Exception;

use Exception;

class NotCompatibleIdException extends Exception
{
}
