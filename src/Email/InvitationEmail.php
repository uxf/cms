<?php

declare(strict_types=1);

namespace UXF\CMS\Email;

use Override;
use UXF\CMS\Mime\PreviewableEmail;

final class InvitationEmail extends PreviewableEmail
{
    public function __construct(
        public readonly string $url,
    ) {
        parent::__construct('@UXFCms/email/cms/invitation.html.twig');
    }

    /**
     * @return iterable<string, self>
     */
    #[Override]
    public static function getPreviewData(): iterable
    {
        return [
            'default' => new self('https://uxf.cz'),
        ];
    }
}
