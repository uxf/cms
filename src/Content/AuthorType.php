<?php

declare(strict_types=1);

namespace UXF\CMS\Content;

use Doctrine\ORM\EntityManagerInterface;
use UXF\CMS\Autocomplete\Adapter\DoctrineAutocomplete;
use UXF\CMS\Autocomplete\AutocompleteBuilder;
use UXF\CMS\Autocomplete\AutocompleteType;
use UXF\CMS\Form\Field\ImageField;
use UXF\Content\Entity\Author;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\DataGridBuilder;
use UXF\DataGrid\DataGridSortDir;
use UXF\DataGrid\DataGridType;
use UXF\DataGrid\DataSource\DataSource;
use UXF\DataGrid\DataSource\DoctrineDataSource;
use UXF\DataGrid\Filter\StringFilter;
use UXF\Form\Field\BasicField;
use UXF\Form\FormBuilder;
use UXF\Form\FormType;
use UXF\Storage\Http\Response\ImageResponse;

/**
 * @template-implements FormType<Author>
 * @template-implements DataGridType<Author>
 */
final readonly class AuthorType implements FormType, DataGridType, AutocompleteType
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function buildForm(FormBuilder $builder): void
    {
        $builder->setClassName(Author::class);
        $builder->addField(new BasicField('firstName', 'string', 'string', 'Jméno'));
        $builder->addField(new BasicField('surname', 'string', 'string', 'Příjmení'));
        $builder->addField(new ImageField('avatar', 'Avatar'));
    }

    /**
     * @param mixed[] $options
     */
    public function buildGrid(DataGridBuilder $builder, array $options = []): void
    {
        $builder->addColumn(new Column('id', 'Id'))->setSort();
        $builder->addColumn(new Column('firstName', 'Jméno'))->setSort();
        $builder->addColumn(new Column('surname', 'Příjmení'))->setSort();

        $builder->addColumn(new Column('avatar', 'Avatar'))
            ->setType('avatar')
            ->setCustomContentCallback(fn (Author $author) => ImageResponse::createNullable($author->getAvatar()));

        $builder->setDefaultSort('surname', DataGridSortDir::ASC);

        $builder->addFilter(new StringFilter('firstName', 'Jméno'));
        $builder->addFilter(new StringFilter('surname', 'Příjmení'));
    }

    /**
     * @param mixed[] $options
     */
    public function getDataSource(array $options = []): DataSource
    {
        $qb = $this->entityManager->createQueryBuilder()
            ->select('author')
            ->from(Author::class, 'author');

        return new DoctrineDataSource($qb);
    }

    /**
     * @param mixed[] $options
     */
    public function buildAutocomplete(AutocompleteBuilder $builder, array $options = []): void
    {
        $builder->setAutocomplete(DoctrineAutocomplete::create($this->entityManager, Author::class, ['firstName', 'surname']));
    }

    public static function getName(): string
    {
        return 'content-author';
    }
}
