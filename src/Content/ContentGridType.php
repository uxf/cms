<?php

declare(strict_types=1);

namespace UXF\CMS\Content;

use Doctrine\ORM\EntityManagerInterface;
use UXF\Content\Entity\Content;
use UXF\Content\Entity\Tag;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\DataGridBuilder;
use UXF\DataGrid\DataGridSortDir;
use UXF\DataGrid\DataGridType;
use UXF\DataGrid\DataSource\DataSource;
use UXF\DataGrid\DataSource\DoctrineDataSource;
use UXF\DataGrid\Filter\EntityMultiSelectFilter;
use UXF\DataGrid\Filter\EntitySelectFilter;
use UXF\DataGrid\Filter\SelectFilter;
use UXF\DataGrid\Filter\StringFilter;
use UXF\DataGrid\Schema\FilterOption;

/**
 * @template-implements DataGridType<Content>
 */
final readonly class ContentGridType implements DataGridType
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /**
     * @inheritDoc
     */
    public function buildGrid(DataGridBuilder $builder, array $options = []): void
    {
        $builder->addColumn(new Column('id', 'Id'))->setSort();
        $builder->addColumn(new Column('type', 'Typ'))->setSort();
        $builder->addColumn(new Column('name', 'Název'))->setSort();
        $builder->addColumn(new Column('author', 'Autor'))
            ->setType('toOne')
            ->setCustomContentCallback(static fn (Content $content) => $content->getAuthor() !== null ? [
                'id' => $content->getAuthor()->getId(),
                'label' => $content->getAuthor()->getFullName(),
            ] : null)
            ->setSort();
        $builder->addColumn(new Column('tags', 'Tagy', 'tags.label'))
            ->setType('toMany')
            ->setCustomContentCallback(static fn (Content $content) => $content->getTags()->map(fn (Tag $tag) => [
                'id' => $tag->getId(),
                'label' => $tag->getLabel(),
            ])->getValues())
            ->setSort();
        $builder->addColumn(new Column('publishedAt', 'Publikováno'))->setType('datetime')->setSort();
        $builder->addColumn(new Column('createdAt', 'Vytvořeno'))->setType('datetime')->setSort();

        $builder->addFilter(new StringFilter('type', 'Typ'));
        $builder->addFilter(new StringFilter('name', 'Název'));
        $builder->addFilter(new SelectFilter('hidden', 'Skryto', [
            new FilterOption(1, 'Ano'),
            new FilterOption(0, 'Ne'),
        ]));
        $builder->addFilter(new EntitySelectFilter('author', 'Autor', 'content-author'));
        $builder->addFilter(new EntityMultiSelectFilter('tags', 'Tagy', 'content-tag'));

        $builder->setDefaultSort('id', DataGridSortDir::DESC);
    }

    /**
     * @inheritDoc
     */
    public function getDataSource(array $options = []): DataSource
    {
        $qb = $this->entityManager->createQueryBuilder()
            ->select('c, author, tags')
            ->from(Content::class, 'c')
            ->leftJoin('c.author', 'author')
            ->leftJoin('c.tags', 'tags');

        return new DoctrineDataSource($qb);
    }

    public static function getName(): string
    {
        return 'content';
    }
}
