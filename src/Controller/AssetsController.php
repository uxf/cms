<?php

declare(strict_types=1);

namespace UXF\CMS\Controller;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Attribute\Route;

final readonly class AssetsController
{
    public function __construct(private string $projectDir)
    {
    }

    #[Route('/assets/{path}', requirements: [
        'path' => '.*',
    ])]
    public function assets(string $path): BinaryFileResponse
    {
        if (str_contains($path, '..')) {
            throw new BadRequestHttpException();
        }

        return self::getFileResponse($this->projectDir . "/public/assets/$path");
    }

    #[Route('/bundles/{path}', requirements: [
        'path' => '.*',
    ])]
    public function bundles(string $path): BinaryFileResponse
    {
        if (str_contains($path, '..')) {
            throw new BadRequestHttpException();
        }

        return self::getFileResponse($this->projectDir . "/public/bundles/$path");
    }

    private static function getFileResponse(string $filename): BinaryFileResponse
    {
        if (!file_exists($filename)) {
            throw new BadRequestHttpException();
        }

        $contentType = match (pathinfo($filename, PATHINFO_EXTENSION)) {
            'css' => 'text/css',
            'js' => 'application/javascript',
            'jpg', 'jpeg' => 'image/jpeg',
            'png' => 'image/png',
            'svg' => 'image/svg+xml',
            default => 'text/plain',
        };

        $response = new BinaryFileResponse($filename);
        $response->headers->set('Content-Type', $contentType);

        return $response;
    }
}
