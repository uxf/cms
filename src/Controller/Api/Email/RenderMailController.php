<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Email;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Twig\Environment;
use UXF\CMS\Service\EmailPreviewService;
use UXF\CMS\Service\EmailRenderer;
use UXF\Core\Exception\BasicException;

readonly class RenderMailController
{
    public function __construct(
        private EmailPreviewService $emailPreviewService,
        private EmailRenderer $emailRenderer,
        private Environment $twig,
    ) {
    }

    #[Route('/api/cms/email/{email}/{variant}', name: 'cms_email_preview', methods: 'GET')]
    public function __invoke(string $email, string $variant): Response
    {
        $className = $this->emailPreviewService->getEmailClasses()[$email] ?? throw BasicException::notFound();

        $message = iterator_to_array($className::getPreviewData())[$variant] ?? throw BasicException::notFound();

        $vars = array_merge($message->getContext(), [
            'email' => $this->emailRenderer,
        ]);
        $html = $this->twig->render($message->getHtmlTemplate() ?? '', $vars);

        return new Response($html);
    }
}
