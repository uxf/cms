<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Cms;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use UXF\CMS\Service\Storage\StorageConfig;
use UXF\Storage\Entity\File;
use UXF\Storage\Http\Response\StorageResponse;
use UXF\Storage\Service\StorageFileService;

final readonly class CmsFileUploadController
{
    public function __construct(
        private StorageFileService $storageFileService,
        private StorageConfig $config,
    ) {
    }

    /**
     * @return StorageResponse[]
     */
    #[Route('/api/cms/files/upload', name: 'cms_file_upload', methods: 'POST')]
    public function __invoke(Request $request): array
    {
        $namespace = $request->get('namespace', $this->config->defaultUploadNamespace);

        $files = array_map(
            fn (UploadedFile $uploadedFile) => $this->storageFileService->createFile($uploadedFile, $namespace),
            $request->files->all()['files'] ?? [],
        );

        return array_map(static fn (File $file) => StorageResponse::from($file), $files);
    }
}
