<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Cms;

use Symfony\Component\Routing\Attribute\Route;
use UXF\CMS\Http\Response\UserResponse;
use UXF\CMS\Security\LoggedUserProvider;
use UXF\CMS\Service\UserResponseProvider;

final readonly class CmsMeController
{
    public function __construct(
        private LoggedUserProvider $userProvider,
        private UserResponseProvider $userResponseProvider,
    ) {
    }

    #[Route('/api/cms/me', name: 'cms_me', methods: 'GET')]
    public function __invoke(): UserResponse
    {
        return $this->userResponseProvider->get($this->userProvider->getUser());
    }
}
