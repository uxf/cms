<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Cms;

use Symfony\Component\Routing\Attribute\Route;
use UXF\CMS\Http\Response\UserInfoResponse;
use UXF\CMS\Security\LoggedUserProvider;
use UXF\CMS\Service\UserResponseProvider;

final readonly class CmsUserController
{
    public function __construct(
        private LoggedUserProvider $userProvider,
        private UserResponseProvider $userResponseProvider,
    ) {
    }

    #[Route('/api/cms/user', name: 'cms_user', methods: 'GET')]
    public function __invoke(): UserInfoResponse
    {
        $user = $this->userProvider->getUser();
        return $this->userResponseProvider->getUserResponse($user);
    }
}
