<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Cms;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Attribute\Route;
use UXF\CMS\Repository\UserRepository;
use UXF\Core\Exception\BasicException;

final readonly class CmsChangeUserActiveController
{
    public function __construct(
        private UserRepository $userRepository,
        private EntityManagerInterface $entityManager,
    ) {
    }

    #[Route('/api/cms/change-user-active/{userId}', name: 'cms_change_user_active', methods: 'POST')]
    public function __invoke(int $userId): void
    {
        $user = $this->userRepository->find($userId);

        if ($user === null) {
            throw new BasicException('Uživatel nenalezen');
        }

        $user->setActive(!$user->isActive());
        $this->entityManager->flush();
    }
}
