<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Cms;

use Symfony\Component\Routing\Attribute\Route;
use UXF\CMS\Entity\User;
use UXF\CMS\Http\Request\RecoveryPassword\SetPasswordRequestBody;
use UXF\CMS\Http\Response\StatusResponse;
use UXF\CMS\Service\PasswordRecovery\PasswordRecovery;
use UXF\Core\Attribute\FromBody;

final readonly class CmsSetUserPasswordController
{
    public function __construct(
        private PasswordRecovery $passwordRecovery,
    ) {
    }

    #[Route('/api/cms/set-password/{user}', name: 'cms_set_password', methods: 'POST')]
    public function __invoke(User $user, #[FromBody] SetPasswordRequestBody $input): StatusResponse
    {
        $this->passwordRecovery->setPassword($user, $input->password);

        return new StatusResponse(true);
    }
}
