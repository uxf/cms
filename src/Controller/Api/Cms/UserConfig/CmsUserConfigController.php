<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Cms\UserConfig;

use Symfony\Component\Routing\Attribute\Route;
use UXF\CMS\Repository\UserConfigRepository;
use UXF\CMS\Security\LoggedUserProvider;

final readonly class CmsUserConfigController
{
    public function __construct(
        private UserConfigRepository $userConfigRepository,
        private LoggedUserProvider $loggedUserProvider,
    ) {
    }

    #[Route('/api/cms/user-config/{name}', name: 'cms_user_config', methods: 'GET')]
    public function __invoke(string $name): mixed
    {
        return $this->userConfigRepository->get($this->loggedUserProvider->getUser(), $name)->getData();
    }
}
