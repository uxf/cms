<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Cms\UserConfig;

use Symfony\Component\Routing\Attribute\Route;
use UXF\CMS\Security\LoggedUserProvider;
use UXF\CMS\Service\UserConfigService;
use UXF\Core\Attribute\FromBody;

final readonly class CmsUserConfigUpdateController
{
    public function __construct(
        private UserConfigService $userConfigService,
        private LoggedUserProvider $loggedUserProvider,
    ) {
    }

    #[Route('/api/cms/user-config/{name}', name: 'cms_user_config_update', methods: 'PUT')]
    public function __invoke(string $name, #[FromBody] mixed $body): mixed
    {
        return $this->userConfigService->save($name, $body, $this->loggedUserProvider->getUser())->getData();
    }
}
