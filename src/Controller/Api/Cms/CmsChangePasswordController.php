<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Cms;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use UXF\CMS\Http\Request\Cms\ChangePasswordRequestBody;
use UXF\CMS\Http\Response\StatusResponse;
use UXF\CMS\Security\LoggedUserProvider;
use UXF\Core\Attribute\FromBody;

final readonly class CmsChangePasswordController
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggedUserProvider $loggedUserProvider,
        private UserPasswordHasherInterface $encoder,
    ) {
    }

    #[Route('/api/cms/change-password', name: 'cms_change_password', methods: 'POST')]
    public function __invoke(#[FromBody] ChangePasswordRequestBody $input): StatusResponse
    {
        $originalPassword = $input->originalPassword;
        $newPassword = $input->newPassword;
        $newPasswordAgain = $input->newPasswordAgain;

        $user = $this->loggedUserProvider->getUser();
        $passwordValid = $this->encoder->isPasswordValid($user, $originalPassword);

        if ($passwordValid && $newPassword === $newPasswordAgain) {
            // set new password
            $encodedPassword = $this->encoder->hashPassword($user, $newPassword);
            $user->setPassword($encodedPassword);
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            return new StatusResponse(true);
        }

        return new StatusResponse(false);
    }
}
