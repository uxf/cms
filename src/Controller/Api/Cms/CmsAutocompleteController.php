<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Cms;

use Symfony\Component\Routing\Attribute\Route;
use UXF\CMS\Http\Request\Cms\CmsAutocompleteRequestQuery;
use UXF\Core\Attribute\FromQuery;
use UXF\Core\Contract\Autocomplete\AutocompleteFactory;
use UXF\Core\Contract\Autocomplete\AutocompleteResult;

final readonly class CmsAutocompleteController
{
    public function __construct(private AutocompleteFactory $autocompleteFactory)
    {
    }

    /**
     * @return AutocompleteResult[]
     */
    #[Route('/api/cms/autocomplete/{name}', name: 'cms_autocomplete', methods: 'GET')]
    public function __invoke(string $name, #[FromQuery] CmsAutocompleteRequestQuery $query): array
    {
        return $this->autocompleteFactory->createAutocomplete($name)->search(trim($query->term), $query->limit);
    }
}
