<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api\Cms;

use Symfony\Component\Routing\Attribute\Route;
use UXF\Core\Contract\Metadata\Metadata;
use UXF\Core\Contract\Metadata\MetadataLoader;

final readonly class CmsTablesController
{
    public function __construct(private MetadataLoader $entityLoader)
    {
    }

    /**
     * @return array<string, Metadata>
     */
    #[Route('/api/cms/tables', name: 'cms_tables', methods: 'GET')]
    public function tables(): array
    {
        return $this->entityLoader->getMetadata();
    }
}
