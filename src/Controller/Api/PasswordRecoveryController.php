<?php

declare(strict_types=1);

namespace UXF\CMS\Controller\Api;

use Symfony\Component\Routing\Attribute\Route;
use UXF\CMS\Http\Request\RecoveryPassword\ForgottenPasswordRequestBody;
use UXF\CMS\Http\Request\RecoveryPassword\PasswordRecoveryRequestBody;
use UXF\CMS\Http\Request\RecoveryPassword\VerifyTokenRequestBody;
use UXF\CMS\Service\PasswordRecovery\PasswordRecovery;
use UXF\Core\Attribute\FromBody;

final readonly class PasswordRecoveryController
{
    public function __construct(private PasswordRecovery $passwordRecovery)
    {
    }

    #[Route('/api/forgotten-password', name: 'api_forgotten_password', methods: 'POST')]
    public function forgottenPassword(#[FromBody] ForgottenPasswordRequestBody $body): void
    {
        $this->passwordRecovery->forgotten($body->email, $body->cms);
    }

    #[Route('/api/password-recovery', name: 'api_password_recovery', methods: 'POST')]
    public function passwordRecovery(#[FromBody] PasswordRecoveryRequestBody $body): void
    {
        $this->passwordRecovery->recovery($body->token, $body->password);
    }

    #[Route('/api/password-recovery-verify-token', name: 'api_password_recovery_verify_token', methods: 'POST')]
    public function verifyToken(#[FromBody] VerifyTokenRequestBody $body): void
    {
        $this->passwordRecovery->verifyToken($body->token);
    }
}
