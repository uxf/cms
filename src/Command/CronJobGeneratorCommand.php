<?php

declare(strict_types=1);

namespace UXF\CMS\Command;

use Nette\Utils\Strings;
use ReflectionClass;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UXF\CMS\Attribute\CronJob;
use UXF\Core\Utils\ClassFinder;

#[AsCommand(name: 'uxf:cron-gen')]
class CronJobGeneratorCommand extends Command
{
    /**
     * @param string[] $sources
     * @param string[] $destinations
     */
    public function __construct(
        private readonly array $sources,
        private readonly array $destinations,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $classNames = ClassFinder::findAllClassNames($this->sources);

        /** @var array<string, string> $commands */
        $commands = [];

        foreach ($classNames as $className) {
            $ref = new ReflectionClass($className);
            $attr = $ref->getAttributes(CronJob::class)[0] ?? null;
            if ($attr === null) {
                continue;
            }

            $cronJob = $attr->newInstance();
            $command = $ref->getAttributes(AsCommand::class)[0]->newInstance();

            $name = trim("{$command->name} {$cronJob->args}");
            $commands[$name] = $cronJob->crontab;
        }

        if ($commands === []) {
            $output->writeln("<error>No commands found</error>");
            return 1;
        }

        ksort($commands);

        $maxLength = max(array_map('strlen', $commands));

        $out = '';
        foreach ($commands as $name => $crontab) {
            $out .= Strings::padRight($crontab, $maxLength) . " /var/www/bin/console {$name} 2>/proc/1/fd/2\n";
        }

        foreach ($this->destinations as $destination) {
            file_put_contents($destination, $out);
        }

        return 0;
    }
}
