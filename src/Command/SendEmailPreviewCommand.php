<?php

declare(strict_types=1);

namespace UXF\CMS\Command;

use Exception;
use Override;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\MailerInterface;
use UXF\CMS\Mime\PreviewableEmail;
use UXF\CMS\Service\EmailPreviewService;

#[AsCommand(name: 'uxf:email:send-preview')]
class SendEmailPreviewCommand extends Command
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly EmailPreviewService $emailPreviewService,
    ) {
        parent::__construct();
    }

    #[Override]
    protected function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'Target email')
            ->addArgument('email_class', InputArgument::OPTIONAL, 'Email class of preview');
    }

    #[Override]
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $targetEmail = $input->getArgument('email');
        $emailClass = $input->getArgument('email_class');

        $emailClasses = $this->emailPreviewService->getEmailClasses();
        if ($emailClass !== null) {
            $emailClass = $emailClasses[$emailClass] ?? throw new Exception('Email not found');
            $this->sendEmailPreview($emailClass, $targetEmail);
        } else {
            foreach ($emailClasses as $class) {
                $this->sendEmailPreview($class, $targetEmail);
            }
        }

        return 0;
    }

    /**
     * @template T of PreviewableEmail
     * @param class-string<T> $className
     */
    private function sendEmailPreview(string $className, string $targetEmail): void
    {
        foreach ($className::getPreviewData() as $variant => $message) {
            $message->to($targetEmail);
            $message->from('root@uxf.dev');
            $message->subject("Testovací email: {$className} - {$variant}");

            $this->mailer->send($message);
        }
    }
}
