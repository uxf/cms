<?php

declare(strict_types=1);

namespace UXF\CMS\Form\Mapper;

use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use UXF\Content\Entity\ContentLite;
use UXF\Core\Exception\ValidationException;
use UXF\Form\Field\Field;
use UXF\Form\Mapper\Mapper;

final readonly class ContentMapper implements Mapper
{
    public const string SERVICE_ID = 'uxf_form.content_mapper';

    public function __construct(private PropertyAccessorInterface $propertyAccessor)
    {
    }

    public function mapToEntityConstructor(Field $field, mixed $value): ContentLite
    {
        if (!isset($value['data'], $value['search'])) {
            throw new ValidationException([[
                'field' => $field->getName(),
                'message' => 'missing data or search term',
            ]]);
        }

        return new ContentLite($value['data'], $value['search']);
    }

    public function mapToEntity(Field $field, object $entity, mixed $value): void
    {
        if (is_array($value) && $this->propertyAccessor->isWritable($entity, $field->getName())) {
            $this->propertyAccessor->setValue($entity, $field->getName(), $this->mapToEntityConstructor($field, $value));
        }
    }

    public function mapFromEntity(Field $field, object $entity): mixed
    {
        if ($this->propertyAccessor->isReadable($entity, $field->getName())) {
            return $this->propertyAccessor->getValue($entity, $field->getName());
        }

        return null;
    }
}
