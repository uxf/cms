<?php

declare(strict_types=1);

namespace UXF\CMS\Form;

use UXF\CMS\Attribute\CmsForm;

final class EntityPropertyInfo
{
    public const int NONE_RELATION = 0;
    public const int EMBEDDED_RELATION = 16;

    public const int ONE_TO_ONE_RELATION = 1;
    public const int MANY_TO_ONE_RELATION = 2;

    public const int ONE_TO_MANY_RELATION = 4;
    public const int MANY_TO_MANY_RELATION = 8;

    public string $label;
    public int $order;
    public ?string $type;
    public bool $readOnly;
    public bool $editable;
    public bool $hidden;
    public bool $required;
    public string $targetField;
    public string $autocomplete;
    public bool $fieldset;

    /* ==== injected properties ==== */

    public string $name;
    public int $relationType = self::NONE_RELATION;
    public ?string $phpType = null;
    public ?string $ormType = null;
    /** @var EntityPropertyInfo[] */
    public array $childrenProperties = [];

    public function __construct(CmsForm $attr, string $name)
    {
        $this->name = $name;
        $this->label = $attr->label !== '' ? $attr->label : $name;
        $this->order = $attr->order;
        $this->type = $attr->type !== '' ? $attr->type : null;
        $this->readOnly = $attr->readOnly;
        $this->editable = $attr->editable;
        $this->hidden = $attr->hidden;
        $this->required = $attr->required;
        $this->targetField = $attr->targetField;
        $this->autocomplete = $attr->autocomplete;
        $this->fieldset = $attr->fieldset;
    }
}
