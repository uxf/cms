<?php

declare(strict_types=1);

namespace UXF\CMS\Form\Field;

use UXF\CMS\Form\Mapper\FileCollectionMapper;
use UXF\Form\Field\Field;
use UXF\Storage\Entity\File;

final class FileCollectionField extends Field
{
    public function __construct(string $name, ?string $label = null)
    {
        parent::__construct($name, File::class, 'files', $label);
    }

    public function getDefaultMapperServiceId(): string
    {
        return FileCollectionMapper::SERVICE_ID;
    }
}
