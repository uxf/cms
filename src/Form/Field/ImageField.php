<?php

declare(strict_types=1);

namespace UXF\CMS\Form\Field;

use UXF\CMS\Form\Mapper\ImageMapper;
use UXF\Form\Field\Field;
use UXF\Storage\Entity\Image;

final class ImageField extends Field
{
    public function __construct(string $name, ?string $label = null)
    {
        parent::__construct($name, Image::class, 'image', $label);
    }

    public function getDefaultMapperServiceId(): string
    {
        return ImageMapper::SERVICE_ID;
    }
}
