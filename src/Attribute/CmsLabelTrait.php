<?php

declare(strict_types=1);

namespace UXF\CMS\Attribute;

use ReflectionEnumBackedCase;

/**
 * @deprecated Please use LabelTrait
 */
trait CmsLabelTrait
{
    public function getLabel(): string
    {
        return (new ReflectionEnumBackedCase($this, $this->name))->getAttributes(CmsLabel::class)[0]->newInstance()->label;
    }
}
