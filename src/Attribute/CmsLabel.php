<?php

declare(strict_types=1);

namespace UXF\CMS\Attribute;

use Attribute;

/**
 * @deprecated Please use Label
 */
#[Attribute(Attribute::TARGET_CLASS_CONSTANT)]
final readonly class CmsLabel
{
    public function __construct(
        public string $label,
        public ?string $color = null,
    ) {
    }
}
