<?php

declare(strict_types=1);

namespace UXF\CMS\Attribute;

use Attribute;

/**
 * @deprecated Please use FormType
 */
#[Attribute(Attribute::TARGET_PROPERTY)]
final class CmsForm
{
    public function __construct(
        public readonly string $label = '',
        public int $order = 0,
        // Allowed values: string, text, markdown, integer, datetime
        public readonly string $type = '',
        public readonly bool $readOnly = false,
        // If FALSE column will be active only for creating, for editing it will be disabled
        public readonly bool $editable = true,
        public readonly bool $hidden = false,
        public readonly bool $required = true,
        public readonly string $targetField = '',
        public readonly string $autocomplete = '',
        public readonly bool $fieldset = false,
    ) {
    }
}
