<?php

declare(strict_types=1);

namespace UXF\CMS\Attribute;

use Attribute;

/**
 * @deprecated Please use GridType
 */
#[Attribute(Attribute::TARGET_PROPERTY)]
final readonly class CmsGrid
{
    public function __construct(
        public string $label = '',
        public int $order = 0,
        // boolean, date, datetime, email, fileSize, integer, phone, string, url, float
        public string $type = '',
        public bool $hidden = false,
        public string $targetColumn = '',
        public string $autocomplete = '',
        public bool $sortable = true,
        public bool $filterable = true,
        // (contains, equals, startsWith, endsWith)
        public string $matchType = 'contains',
        public bool $filterMulti = false,
    ) {
    }
}
