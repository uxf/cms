<?php

declare(strict_types=1);

namespace UXF\CMS\Service;

use Doctrine\ORM\EntityManagerInterface;
use UXF\CMS\Attribute\CmsMeta;

final class CmsMetaAnnotationResolver
{
    /** @var array<string, CmsMeta>|null */
    private ?array $map = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    public function findCmsMetaByClassName(string $className): ?CmsMeta
    {
        return $this->getCmsMetas()[$className] ?? null;
    }

    /**
     * @return array<string, CmsMeta>
     */
    public function getCmsMetas(): array
    {
        if ($this->map === null) {
            $this->map = [];

            foreach ($this->entityManager->getMetadataFactory()->getAllMetadata() as $metadata) {
                $rc = $metadata->getReflectionClass();

                $attr = $rc->getAttributes(CmsMeta::class)[0] ?? null;
                $cmsMeta = $attr?->newInstance();
                if ($cmsMeta instanceof CmsMeta) {
                    $this->map[$metadata->name] = $cmsMeta;
                }
            }
        }

        return $this->map;
    }
}
