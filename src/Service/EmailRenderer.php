<?php

declare(strict_types=1);

namespace UXF\CMS\Service;

use Twig\Environment;

readonly class EmailRenderer
{
    public function __construct(
        private Environment $twig,
        private string $projectDir,
    ) {
    }

    public function image(string $image, ?string $contentType = null): string
    {
        $file = $this->twig->getLoader()->getSourceContext($image);
        return str_replace($this->projectDir . '/public', '', $file->getPath());
    }
}
