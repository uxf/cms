<?php

declare(strict_types=1);

namespace UXF\CMS\Service;

use UXF\CMS\Entity\Role;
use UXF\CMS\Entity\User;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Email;

interface UserCreator
{
    /**
     * @param Role[] $roles
     */
    public function createUser(
        Email $email,
        string $name,
        ?string $surname,
        array $roles,
        bool $sendInvitation = true,
        ?DateTime $expiration = null,
    ): User;
}
