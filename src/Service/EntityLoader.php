<?php

declare(strict_types=1);

namespace UXF\CMS\Service;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\CacheItemPoolInterface;
use ReflectionClass;
use UXF\CMS\Attribute\CmsMeta;
use UXF\CMS\Utils\Exception\TableAnnotationException;
use UXF\Core\Contract\Metadata\Metadata;
use UXF\Core\Contract\Metadata\MetadataLoader;

/**
 * @deprecated
 */
final class EntityLoader implements MetadataLoader
{
    /** @var array<string, Metadata>|null */
    private ?array $metadata = null;

    public function __construct(
        private readonly bool $debug,
        private readonly EntityManagerInterface $em,
        private readonly CacheItemPoolInterface $cache,
    ) {
    }

    /**
     * @return array<string, Metadata>
     */
    public function getMetadata(): array
    {
        if ($this->metadata !== null) {
            return $this->metadata;
        }

        $cacheItem = $this->cache->getItem('EntityLoader_EntitiesMetadata');

        if ($this->debug === false && $cacheItem->isHit()) {
            return $this->metadata = $cacheItem->get();
        }

        // L2 cache miss
        $this->metadata = [
            'content-tag' => new Metadata(
                title: 'Štítky',
                entityAlias: 'content-tag',
                actions: ['add', 'edit', 'remove'],
            ),
            'content-author' => new Metadata(
                title: 'Autoři',
                entityAlias: 'content-author',
                actions: ['add', 'edit', 'remove'],
            ),
            'content-category' => new Metadata(
                title: 'Kategorie',
                entityAlias: 'content-category',
                actions: ['add', 'edit', 'remove'],
            ),
            'content' => new Metadata(
                title: 'Content',
                entityAlias: 'content',
                actions: ['add', 'edit', 'remove'],
            ),
        ];

        foreach ($this->em->getMetadataFactory()->getAllMetadata() as $entity) {
            $ref = new ReflectionClass($entity->getName());
            $attr = $ref->getAttributes(CmsMeta::class)[0] ?? null;
            $classMetaAnnotation = $attr?->newInstance();
            if (!$classMetaAnnotation instanceof CmsMeta) {
                continue;
            }

            if ($classMetaAnnotation->title === '' || $classMetaAnnotation->alias === '') {
                throw new TableAnnotationException(CmsMeta::class . ' $alias and $title are required!');
            }

            if (isset($this->metadata[$classMetaAnnotation->alias])) {
                throw new TableAnnotationException("Duplicity entityAlias: '{$classMetaAnnotation->alias}'");
            }

            $this->metadata[$classMetaAnnotation->alias] = new Metadata(
                title: $classMetaAnnotation->title,
                entityAlias: $classMetaAnnotation->alias,
                actions: $classMetaAnnotation->actions,
            );
        }

        $cacheItem->set($this->metadata);
        $this->cache->save($cacheItem);

        return $this->metadata;
    }
}
