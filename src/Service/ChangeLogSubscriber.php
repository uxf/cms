<?php

declare(strict_types=1);

namespace UXF\CMS\Service;

use BackedEnum;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Psr\Log\LoggerInterface;
use Stringable;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Throwable;
use UXF\CMS\Contract\NormalizedLoggable;
use UXF\CMS\Entity\ChangeLog;
use UXF\CMS\Entity\User;
use UXF\CMS\Exception\NotCompatibleIdException;
use UXF\CMS\Security\LoggedUserProvider;
use UXF\Core\Type\Equals;
use UXF\Core\Utils\ClassNameHelper;

#[AsDoctrineListener(Events::onFlush)]
final readonly class ChangeLogSubscriber
{
    /**
     * @param array<string, string[]> $entities
     */
    public function __construct(
        private array $entities,
        private LoggedUserProvider $loggedUserProvider,
        private LoggerInterface $logger,
    ) {
    }

    private function getLoggedUserId(): ?int
    {
        try {
            return $this->loggedUserProvider->getUser()->getId();
        } catch (AccessDeniedHttpException) {
            return null;
        }
    }

    public function getEntityId(object $entity): string
    {
        if (method_exists($entity, 'getId')) {
            try {
                return (string) $entity->getId();
            } catch (Throwable $e) {
                throw new NotCompatibleIdException($e->getMessage());
            }
        }

        throw new NotCompatibleIdException("ChangeLog: can not find 'getId' method.");
    }

    public function onFlush(OnFlushEventArgs $eventArgs): void
    {
        $em = $eventArgs->getObjectManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            $entityName = ClassNameHelper::normalizeClassName($entity::class);

            $loggedAttributes = $this->entities[$entityName] ?? [];

            if ($loggedAttributes === []) {
                continue;
            }

            try {
                $changeSet = $uow->getEntityChangeSet($entity);

                $values = [];
                foreach ($loggedAttributes as $attribute) {
                    if (isset($changeSet[$attribute]) && is_array($changeSet[$attribute])) {
                        [$from, $to] = $changeSet[$attribute];
                        if ($from instanceof Equals && $to instanceof Equals && $to instanceof $from && $from->equals($to)) {
                            continue; // skip equal values
                        }

                        $this->addChangedAttributeToValues(
                            $values,
                            $attribute,
                            $this->normalize($to, $entityName, $attribute),
                        );
                    }
                }

                if ($values === []) {
                    continue;
                }

                $userId = $this->getLoggedUserId();
                $user = $userId !== null ? $em->getReference(User::class, $userId) : null;
                $changeLog = new ChangeLog($user, $entityName, $this->getEntityId($entity), $values);

                $em->persist($changeLog);

                $metadata = $em->getClassMetadata(ChangeLog::class);
                $uow->computeChangeSet($metadata, $changeLog);
            } catch (NotCompatibleIdException $e) {
                $this->logger->error($e->getMessage() . " Entity name: {$entityName}");
            }
        }
    }


    /**
     * @param array<string, mixed> $values
     */
    private function addChangedAttributeToValues(array &$values, string $attributeName, mixed $value): void
    {
        $parts = explode('.', $attributeName);
        $actualValue = &$values;
        foreach ($parts as $i => $part) {
            if ($i < count($parts) - 1) {
                $actualValue = &$actualValue[$part];
            } else {
                $actualValue[$part] = $value;
            }
        }
    }

    /**
     * @return array<string, mixed>|int|float|bool|string|null
     */
    private function normalize(mixed $value, string $entityName, string $attribute): array|int|float|bool|string|null
    {
        if (is_scalar($value) || $value === null) {
            return $value;
        }

        if ($value instanceof BackedEnum) {
            return $value->value;
        }

        if ($value instanceof Stringable) {
            return (string) $value;
        }

        if ($value instanceof DateTimeInterface) {
            return $value->format(DATE_ATOM);
        }

        if ($value instanceof NormalizedLoggable) {
            return $value->normalize();
        }

        if ($value instanceof User) {
            return $value->getEmail()->toString();
        }

        $this->logger->error("ChangeLog: attribute '{$attribute}' in entity '{$entityName}' can not be normalized.");

        return null;
    }
}
