<?php

declare(strict_types=1);

namespace UXF\CMS\Service;

use Doctrine\ORM\EntityManagerInterface;
use UXF\CMS\Entity\User;
use UXF\CMS\Entity\UserConfig;
use UXF\CMS\Repository\UserConfigRepository;

final readonly class UserConfigService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private UserConfigRepository $userConfigRepository,
    ) {
    }

    public function save(string $name, mixed $data, User $user): UserConfig
    {
        $userConfig = $this->userConfigRepository->findByName($user, $name);

        if ($userConfig === null) {
            $userConfig = new UserConfig($user, $name, $data);
            $this->entityManager->persist($userConfig);
        } else {
            $userConfig->setData($data);
        }
        $this->entityManager->flush();

        return $userConfig;
    }
}
