<?php

declare(strict_types=1);

namespace UXF\CMS\Service;

use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use UXF\CMS\Entity\User;
use UXF\CMS\Http\Response\UserInfoResponse;
use UXF\CMS\Http\Response\UserResponse;

final readonly class UserResponseProvider
{
    public function __construct(
        private RoleHierarchyInterface $roleHierarchy,
    ) {
    }

    /**
     * @deprecated
     */
    public function getUserResponse(User $user): UserInfoResponse
    {
        return new UserInfoResponse($this->get($user), []);
    }

    public function get(User $user): UserResponse
    {
        return new UserResponse(
            id: $user->getId(),
            email: $user->getEmail(),
            name: $user->getName(),
            roles: $this->roleHierarchy->getReachableRoleNames($user->getRoles()),
        );
    }
}
