<?php

declare(strict_types=1);

namespace UXF\CMS\Service;

use Doctrine\ORM\EntityManagerInterface;
use Nette\Utils\Random;
use UXF\CMS\Entity\Role;
use UXF\CMS\Entity\User;
use UXF\CMS\Repository\UserRepository;
use UXF\CMS\Service\PasswordRecovery\PasswordRecovery;
use UXF\Core\Exception\BasicException;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Email;

final readonly class UserCreatorService implements UserCreator
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private UserRepository $userRepository,
        private PasswordRecovery $passwordRecovery,
    ) {
    }

    /**
     * @param Role[] $roles
     */
    public function createUser(
        Email $email,
        string $name,
        ?string $surname,
        array $roles,
        bool $sendInvitation = true,
        ?DateTime $expiration = null,
    ): User {
        if ($this->userRepository->findByUsername($email->toString()) !== null) {
            throw new BasicException('Email already used');
        }

        $user = new User($email, $name, Random::generate(), $roles);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        if ($sendInvitation) {
            $this->passwordRecovery->invitation($user->getEmail(), $expiration);
        }

        return $user;
    }
}
