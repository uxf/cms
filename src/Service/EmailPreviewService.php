<?php

declare(strict_types=1);

namespace UXF\CMS\Service;

use ReflectionClass;
use UXF\CMS\Mime\PreviewableEmail;
use UXF\Core\Utils\ClassFinder;

readonly class EmailPreviewService
{
    /**
     * @param string[] $sources
     */
    public function __construct(private array $sources)
    {
    }

    /**
     * @return array<string, class-string<PreviewableEmail>>
     */
    public function getEmailClasses(): array
    {
        if ($this->sources === []) {
            return [];
        }
        $classNames = ClassFinder::findAllClassNames($this->sources);

        $classes = [];

        foreach ($classNames as $className) {
            if (
                is_a($className, PreviewableEmail::class, true) &&
                !(new ReflectionClass($className))->isAbstract()
            ) {
                $classes[$this->serializeClassName($className)] = $className;
            }
        }

        return $classes;
    }

    private function serializeClassName(string $class): string
    {
        return str_replace('\\', '_', $this->camelToKebab($class));
    }

    private function camelToKebab(string $input): string
    {
        $pattern = '/(?<=\\w)(?=[A-Z])|(?<=[a-z])(?=[0-9])/';
        $snakeCase = preg_replace($pattern, '-', $input);
        return strtolower($snakeCase ?? '');
    }
}
