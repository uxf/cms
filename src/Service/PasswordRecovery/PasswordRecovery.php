<?php

declare(strict_types=1);

namespace UXF\CMS\Service\PasswordRecovery;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use UXF\CMS\Email\InvitationEmail;
use UXF\CMS\Email\RecoveryCmsEmail;
use UXF\CMS\Email\RecoveryEmail;
use UXF\CMS\Entity\Invitation\PasswordRecovery as PasswordRecoveryEntity;
use UXF\CMS\Entity\Invitation\UserInvitation;
use UXF\CMS\Entity\User;
use UXF\CMS\Mime\PreviewableEmail;
use UXF\CMS\Repository\UserInvitationRepository;
use UXF\CMS\Repository\UserRepository;
use UXF\Core\Exception\BasicException;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Email;

final readonly class PasswordRecovery
{
    public function __construct(
        private MailerInterface $mailer,
        private UserPasswordHasherInterface $passwordEncoder,
        private UserRepository $userRepository,
        private LoggerInterface $logger,
        private EntityManagerInterface $entityManager,
        private UserInvitationRepository $passwordRecoveryRepository,
        private PasswordRecoveryConfig $config,
    ) {
    }

    private function sendEmail(UserInvitation $passwordRecovery, PasswordRecoveryType $type): void
    {
        $url = $this->config->getRecoveryUrl($type) . $passwordRecovery->getKey();

        $message = $this->getPasswordRecoveryEmail($type, $url);
        $message->from(new Address($this->config->getEmailFrom(), $this->config->getEmailName()));
        $message->to($passwordRecovery->getEmail()->toString());
        $message->subject($this->config->getRecoveryEmailSubject($type));
        $this->mailer->send($message);
    }

    private function getPasswordRecoveryEmail(PasswordRecoveryType $type, string $url): PreviewableEmail
    {
        return match ($type) {
            PasswordRecoveryType::INVITATION_TYPE => new InvitationEmail($url),
            PasswordRecoveryType::RECOVERY_TYPE => new RecoveryEmail($url),
            PasswordRecoveryType::RECOVERY_CMS_TYPE => new RecoveryCmsEmail($url),
        };
    }

    /**
     * Save Password recovery and send email with link
     */
    public function forgotten(Email $email, bool $isCms = false, ?DateTime $expiration = null): void
    {
        $user = $this->userRepository->findByUsername($email->toString());

        if (!$user instanceof User) {
            // TODO: max 10 invalid emails per one IP address?
            throw new BasicException('User not found!');
        }

        try {
            $recoveryEntity = new PasswordRecoveryEntity($email, validity: $expiration);
            $this->entityManager->persist($recoveryEntity);
            $this->sendEmail(
                $recoveryEntity,
                $isCms ? PasswordRecoveryType::RECOVERY_CMS_TYPE : PasswordRecoveryType::RECOVERY_TYPE,
            );
            $this->entityManager->flush();
        } catch (Exception $e) {
            $this->logger->critical("Password recovery: send email error: '$email'!");
            throw $e;
        }
    }

    /**
     * Send invitation to user
     */
    public function invitation(Email $email, ?DateTime $expiration = null): void
    {
        try {
            $recoveryEntity = new UserInvitation($email, validity: $expiration);
            $this->entityManager->persist($recoveryEntity);
            $this->sendEmail($recoveryEntity, PasswordRecoveryType::INVITATION_TYPE);
            $this->entityManager->flush();
        } catch (Exception $e) {
            $this->logger->critical("Password recovery: send email error: '$email'!");
            throw $e;
        }
    }

    public function recovery(string $token, string $password): User
    {
        $passwordRecovery = $this->verifyToken($token);

        $user = $this->userRepository->findByUsername($passwordRecovery->getEmail()->toString());

        if ($user === null) {
            $this->logger->critical('User not found by password recovery! [' . $passwordRecovery->getEmail() . ']');
            throw new BasicException('User not found!');
        }

        $user->setPassword($this->passwordEncoder->hashPassword($user, $password));

        $this->entityManager->remove($passwordRecovery);
        $this->entityManager->flush();

        return $user;
    }

    public function verifyToken(string $token): UserInvitation
    {
        $passwordRecovery = $this->passwordRecoveryRepository->findByToken($token);

        if ($passwordRecovery === null) {
            $this->logger->error("Password recovery token not found! [$token]");
            throw new BasicException('Password recovery token not found!');
        }

        if ($passwordRecovery->getValidity() < Clock::now()) {
            throw new BasicException('Password recovery token expired!');
        }

        return $passwordRecovery;
    }

    public function setPassword(User $user, string $password): User
    {
        $user->setPassword($this->passwordEncoder->hashPassword($user, $password));
        $this->entityManager->flush();

        return $user;
    }
}
