<?php

declare(strict_types=1);

namespace UXF\CMS\Service\PasswordRecovery;

enum PasswordRecoveryType
{
    case INVITATION_TYPE;
    case RECOVERY_TYPE;
    case RECOVERY_CMS_TYPE;
}
