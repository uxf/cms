<?php

declare(strict_types=1);

namespace UXF\CMS\Contract;

interface NormalizedLoggable
{
    /**
     * @return array<string, mixed>
     */
    public function normalize(): array;
}
