<?php

declare(strict_types=1);

namespace UXF\CMS\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use UXF\CMS\Autocomplete\AutocompleteType;

final readonly class AutocompleteTypePass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container): void
    {
        foreach ($container->findTaggedServiceIds('uxf.autocomplete.type') as $id => $tag) {
            $definition = $container->getDefinition($id)
                ->setPublic(true)
                ->setAutowired(true);

            $class = $definition->getClass();
            assert(is_string($class) && is_a($class, AutocompleteType::class, true));

            $container->setAlias('autocomplete.' . $class::getName(), $id)->setPublic(true);
        }
    }
}
