<?php

declare(strict_types=1);

namespace UXF\CMS\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

final readonly class ErrorListenerCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $sentryEnabled = $container->getParameter('uxf_cms.sentry.enabled');

        if ($container->getParameter('kernel.environment') === 'prod' && $sentryEnabled === true) {
            $container->getDefinition('exception_listener')
                ->setArgument('$logger', null); // disable kernel logging
        }
    }
}
