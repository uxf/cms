<?php

declare(strict_types=1);

namespace UXF\CMS\DataGrid;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\AssociationMapping;
use Doctrine\ORM\Mapping\ClassMetadata;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use ReflectionNamedType;
use RuntimeException;
use UXF\CMS\Attribute\CmsGrid;
use UXF\CMS\Attribute\CmsMeta;
use UXF\CMS\Service\CmsMetaAnnotationResolver;
use UXF\Core\Attribute\AutocompleteFields;

// TODO make final
class EntityAnnotationResolver
{
    /** @var array<string, EntityInfo>|null */
    private ?array $entitiesMetadata = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly CmsMetaAnnotationResolver $cmsMetaAnnotationResolver,
        private readonly ContainerInterface $container,
    ) {
    }

    public function findEntityMetadata(string $entityAlias): ?EntityInfo
    {
        return $this->getEntitiesMetadata()[$entityAlias] ?? null;
    }

    /**
     * @return array<string, EntityInfo>
     */
    public function getEntitiesMetadata(): array
    {
        if ($this->entitiesMetadata === null) {
            $entities = [];

            foreach ($this->entityManager->getMetadataFactory()->getAllMetadata() as $metadata) {
                $rc = $metadata->getReflectionClass();

                $attr = $rc->getAttributes(CmsMeta::class)[0] ?? null;
                $cmsMeta = $attr?->newInstance();
                if (!$cmsMeta instanceof CmsMeta) {
                    continue;
                }

                $propertyInfos = [];
                $orderCounter = 0;

                foreach ($rc->getProperties() as $reflectionProperty) {
                    $attr = $reflectionProperty->getAttributes(CmsGrid::class)[0] ?? null;
                    $cmsTableColumn = $attr?->newInstance();
                    if (!$cmsTableColumn instanceof CmsGrid) {
                        continue;
                    }

                    $propertyName = $reflectionProperty->name;
                    $propertyInfo = new EntityPropertyInfo($cmsTableColumn, $propertyName);

                    // fix stable order
                    $propertyInfo->order = ($cmsTableColumn->order !== 0 ? $cmsTableColumn->order : $orderCounter++) * 100;

                    if (isset($metadata->embeddedClasses[$propertyName])) {
                        array_push(
                            $propertyInfos,
                            ...$this->getEmbeddedColumns(
                                $metadata->embeddedClasses[$propertyName]['class'],
                                $propertyName,
                                $propertyInfo->order,
                            ),
                        );
                        continue;
                    }

                    if (isset($metadata->fieldMappings[$propertyName])) {
                        $propertyInfo->ormType = $metadata->fieldMappings[$propertyName]['type'];
                    } elseif (isset($metadata->associationMappings[$propertyName])) {
                        $associationMapping = $metadata->associationMappings[$propertyName];
                        // @phpstan-ignore instanceof.alwaysTrue
                        $type = $associationMapping instanceof AssociationMapping ? $associationMapping->type() : $associationMapping['type'];

                        $propertyInfo->relationType = match ($type) {
                            ClassMetadata::MANY_TO_ONE => EntityPropertyInfo::MANY_TO_ONE_RELATION,
                            ClassMetadata::MANY_TO_MANY => EntityPropertyInfo::MANY_TO_MANY_RELATION,
                            ClassMetadata::ONE_TO_MANY => EntityPropertyInfo::ONE_TO_MANY_RELATION,
                            ClassMetadata::ONE_TO_ONE => EntityPropertyInfo::ONE_TO_ONE_RELATION,
                            default => throw new RuntimeException(),
                        };

                        if ($propertyInfo->autocomplete === '') {
                            $targetClassName = $metadata->associationMappings[$propertyName]['targetEntity'];

                            $autocomplete = ((new ReflectionClass($targetClassName))->getAttributes(AutocompleteFields::class)[0] ?? null)?->newInstance();
                            if ($autocomplete instanceof AutocompleteFields) {
                                $propertyInfo->autocomplete = $autocomplete->name;
                            } else {
                                $targetCmsMeta = $this->cmsMetaAnnotationResolver->findCmsMetaByClassName($targetClassName);
                                if (
                                    $targetCmsMeta instanceof CmsMeta &&
                                    ($targetCmsMeta->autocompleteFields !== [] || $this->container->has("autocomplete.$targetCmsMeta->alias"))
                                ) {
                                    $propertyInfo->autocomplete = $targetCmsMeta->alias;
                                }
                            }
                        }
                    } else {
                        throw new RuntimeException();
                    }

                    $ref = $metadata->reflFields[$propertyName]?->getType();
                    assert($ref instanceof ReflectionNamedType);
                    $propertyInfo->phpType = $ref->getName();

                    $propertyInfos[] = $propertyInfo;
                }

                /** @var class-string<object> & literal-string $className */
                $className = $rc->getName();
                $entities[$cmsMeta->alias] = new EntityInfo($className, $propertyInfos);
            }

            $this->entitiesMetadata = $entities;
        }

        return $this->entitiesMetadata;
    }

    /**
     * @template T of object
     * @phpstan-param class-string<T> $className
     * @return EntityPropertyInfo[]
     */
    private function getEmbeddedColumns(string $className, string $parentPropertyPath, int $parentOrder): array
    {
        $propertyInfos = [];

        $metadata = $this->entityManager->getMetadataFactory()->getMetadataFor($className);
        $rc = $metadata->getReflectionClass();

        $orderCounter = 0;
        foreach ($rc->getProperties() as $reflectionProperty) {
            $attr = $reflectionProperty->getAttributes(CmsGrid::class)[0] ?? null;
            $cmsTableColumn = $attr?->newInstance();
            if (!$cmsTableColumn instanceof CmsGrid) {
                continue;
            }

            $propertyName = $reflectionProperty->getName();

            // embedded in embedded
            if (isset($metadata->embeddedClasses[$propertyName])) {
                array_push(
                    $propertyInfos,
                    ...$this->getEmbeddedColumns(
                        $metadata->embeddedClasses[$propertyName]['class'],
                        "$parentPropertyPath.$propertyName",
                        $parentOrder,
                    ),
                );
                continue;
            }

            $propertyInfo = new EntityPropertyInfo($cmsTableColumn, "$parentPropertyPath.$propertyName");

            $ref = $metadata->reflFields[$propertyName]?->getType();
            assert($ref instanceof ReflectionNamedType);
            $propertyInfo->phpType = $ref->getName();
            $propertyInfo->relationType = EntityPropertyInfo::EMBEDDED_RELATION;
            $propertyInfo->ormType = $metadata->fieldMappings[$propertyName]['type'];

            // fix stable order
            $propertyInfo->order = ($cmsTableColumn->order !== 0 ? $cmsTableColumn->order : $orderCounter++) + $parentOrder;

            $propertyInfos[] = $propertyInfo;
        }

        return $propertyInfos;
    }
}
