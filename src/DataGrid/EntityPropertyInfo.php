<?php

declare(strict_types=1);

namespace UXF\CMS\DataGrid;

use UXF\CMS\Attribute\CmsGrid;

final class EntityPropertyInfo
{
    public const int NONE_RELATION = 0;
    public const int EMBEDDED_RELATION = 16;

    public const int ONE_TO_ONE_RELATION = 1;
    public const int MANY_TO_ONE_RELATION = 2;

    public const int ONE_TO_MANY_RELATION = 4;
    public const int MANY_TO_MANY_RELATION = 8;

    public string $label;
    public int $order;
    public string $type;
    public bool $hidden;
    public string $targetColumn;
    public string $autocomplete;
    public bool $sortable;
    public bool $filterable;
    public bool $filterMulti;
    public string $matchType;

    /* ==== injected properties ==== */

    public string $name;
    public int $relationType = self::NONE_RELATION;
    public ?string $phpType = null;
    public ?string $ormType = null;

    public function __construct(CmsGrid $attr, string $name)
    {
        $this->name = $name;
        $this->label = $attr->label !== '' ? $attr->label : $name;
        $this->order = $attr->order;
        $this->type = $attr->type;
        $this->hidden = $attr->hidden;
        $this->targetColumn = $attr->targetColumn;
        $this->autocomplete = $attr->autocomplete;
        $this->sortable = $attr->sortable;
        $this->filterable = $attr->filterable;
        $this->filterMulti = $attr->filterMulti;
        $this->matchType = $attr->matchType;
    }
}
