<?php

declare(strict_types=1);

namespace UXF\CMS\DataGrid;

use Closure;
use UXF\DataGrid\Column\Column;
use UXF\DataGrid\Column\ColumnConfig;
use UXF\DataGrid\ColumnTrait;
use UXF\Storage\Entity\Image;
use UXF\Storage\Http\Response\ImageResponse;

trait CmsColumnTrait
{
    use ColumnTrait;

    /**
     * @template T
     * @param Closure(T): (Image|null) $fn
     * @return Column<T>
     */
    public static function avatar(string $name, string $label, Closure $fn): Column
    {
        $column = new Column($name, $label);
        $column
            ->setType('avatar')
            ->setCustomContentCallback(fn ($entity) => ImageResponse::createNullable($fn($entity)))
            ->setConfig(new ColumnConfig(width: 50));

        return $column;
    }
}
