<?php

declare(strict_types=1);

namespace UXF\CMS\DataGrid;

final readonly class EntityInfo
{
    /**
     * @param class-string<object> & literal-string $className
     * @param EntityPropertyInfo[] $properties
     */
    public function __construct(
        private string $className,
        private array $properties,
    ) {
    }

    /**
     * @return class-string<object> & literal-string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @return EntityPropertyInfo[]
     */
    public function getProperties(): array
    {
        return $this->properties;
    }
}
