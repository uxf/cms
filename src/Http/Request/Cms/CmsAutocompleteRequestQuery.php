<?php

declare(strict_types=1);

namespace UXF\CMS\Http\Request\Cms;

final readonly class CmsAutocompleteRequestQuery
{
    public function __construct(
        public string $term = '',
        public int $limit = 20,
    ) {
    }
}
