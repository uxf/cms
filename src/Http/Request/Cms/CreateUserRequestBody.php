<?php

declare(strict_types=1);

namespace UXF\CMS\Http\Request\Cms;

use Symfony\Component\Validator\Constraints as Assert;
use UXF\CMS\Entity\Role;
use UXF\Core\Type\Email;

final readonly class CreateUserRequestBody
{
    /**
     * @param Role[] $roles
     */
    public function __construct(
        public Email $email,
        #[Assert\NotBlank]
        public string $name,
        #[Assert\Count(min: 1)]
        public array $roles,
        public ?string $surname = null,
        public ?bool $sendInvitation = null,
    ) {
    }
}
