<?php

declare(strict_types=1);

namespace UXF\CMS\Http\Response;

/**
 * @deprecated
 */
final readonly class UserInfoResponse
{
    /**
     * @param mixed[] $userConfig
     */
    public function __construct(
        public UserResponse $user,
        public array $userConfig,
    ) {
    }
}
