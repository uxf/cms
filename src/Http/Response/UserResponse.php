<?php

declare(strict_types=1);

namespace UXF\CMS\Http\Response;

use UXF\Core\Type\Email;

final readonly class UserResponse
{
    /**
     * @param string[] $roles
     */
    public function __construct(
        public int $id,
        public Email $email,
        public string $name,
        public array $roles,
    ) {
    }
}
