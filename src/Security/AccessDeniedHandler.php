<?php

declare(strict_types=1);

namespace UXF\CMS\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;
use UXF\Core\Http\Response\ErrorResponse;

/**
 * @deprecated will be removed in 4.0 (without replacement)
 */
final class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    public function handle(Request $request, AccessDeniedException $accessDeniedException): Response
    {
        return ErrorResponse::forbidden();
    }
}
