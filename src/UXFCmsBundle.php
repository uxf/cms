<?php

declare(strict_types=1);

namespace UXF\CMS;

use Psr\Log\LogLevel;
use Sentry\State\HubInterface;
use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use UXF\CMS\Autocomplete\AutocompleteType;
use UXF\CMS\Controller\Api\AuditController;
use UXF\CMS\DependencyInjection\AutocompleteTypePass;
use UXF\CMS\DependencyInjection\ChangeLogCompilerPass;
use UXF\CMS\DependencyInjection\ErrorListenerCompilerPass;
use UXF\CMS\EventSubscriber\MailerTagListener;
use UXF\CMS\Service\EmailPreviewService;
use UXF\CMS\Service\PasswordRecovery\PasswordRecoveryConfig;
use UXF\CMS\Service\Storage\StorageConfig;

final class UXFCmsBundle extends AbstractBundle
{
    protected string $extensionAlias = 'uxf_cms';

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
            ->scalarNode('audit_token')->end()
            ->arrayNode('mailing')
                ->isRequired()
                ->children()
                    ->scalarNode('email_from')->isRequired()->end()
                    ->scalarNode('email_name')->isRequired()->end()
                    ->scalarNode('invitation_url')->isRequired()->end()
                    ->scalarNode('recovery_url')->isRequired()->end()
                    ->scalarNode('cms_recovery_url')->isRequired()->end()
                    ->scalarNode('tag')->end()
                    ->arrayNode('email_previews')
                        ->beforeNormalization()
                            ->ifString()
                            ->then(fn (string $v) => [$v])
                        ->end()
                        ->scalarPrototype()->isRequired()->end()
                        ->defaultValue([])
                    ->end()
                ->end()
            ->end()
            ->arrayNode('storage')
                ->children()
                    ->scalarNode('default_upload_namespace')->defaultValue(null)->end()
                ->end()
            ->end()
            ->arrayNode('cron')
                ->addDefaultsIfNotSet()
                ->children()
                    ->scalarNode('stage')->defaultValue('%env(default:uxf_cms.sentry.environment:APP_STAGE)%')->end()
                    ->arrayNode('sources')
                        ->scalarPrototype()->end()
                        ->defaultValue(['%kernel.project_dir%/src'])
                    ->end()
                    ->arrayNode('destinations')
                        ->scalarPrototype()->end()
                        ->defaultValue(['%kernel.project_dir%/docker/cron'])
                    ->end()
                    ->arrayNode('enabled_stages')
                        ->scalarPrototype()->end()
                        ->defaultValue(['prod', 'dev'])
                    ->end()
                ->end()
            ->end()
            ->arrayNode('logger')
                ->children()
                    ->arrayNode('stdout')
                        ->children()
                            ->booleanNode('enabled')->defaultValue(false)->end()
                            ->scalarNode('level')->defaultValue('notice')->end()
                        ->end()
                    ->end()
                    ->arrayNode('sentry')
                        ->children()
                            ->scalarNode('dsn')->isRequired()->end()
                            ->scalarNode('environment')->isRequired()->end()
                            ->scalarNode('release')->cannotBeEmpty()->defaultValue('none')->end()
                            ->scalarNode('level')->defaultValue('warning')->end()
                            ->floatNode('traces_sample_rate')->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
            ->arrayNode('loggable')
                ->addDefaultsIfNotSet()
                ->children()
                    ->arrayNode('sources')
                        ->beforeNormalization()
                            ->ifString()
                            ->then(fn (string $v) => [$v])
                        ->end()
                        ->scalarPrototype()->isRequired()->end()
                        ->defaultValue([])
                    ->end()
                ->end()
            ->end();
    }

    /**
     * @param array<mixed> $config
     */
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import(__DIR__ . '/../config/services.php');

        $services = $container->services();
        $services->set(PasswordRecoveryConfig::class)
            ->arg('$invitationUrl', $config['mailing']['invitation_url'])
            ->arg('$recoveryUrl', $config['mailing']['recovery_url'])
            ->arg('$cmsRecoveryUrl', $config['mailing']['cms_recovery_url'])
            ->arg('$emailFrom', $config['mailing']['email_from'])
            ->arg('$emailName', $config['mailing']['email_name']);

        $services->set(MailerTagListener::class)
            ->arg('$tag', $config['mailing']['tag'] ?? '')
            ->autoconfigure();

        $services->set(StorageConfig::class)
            ->arg('$defaultUploadNamespace', $config['storage']['default_upload_namespace'] ?? 'default');

        $services->set(AuditController::class)
            ->arg('$token', $config['audit_token'] ?? (string) random_int(999_999_999, 999_999_999_999))
            ->public();

        $builder->registerForAutoconfiguration(AutocompleteType::class)
            ->addTag('uxf.autocomplete.type');

        $services->set(EmailPreviewService::class)
            ->arg('$sources', $config['mailing']['email_previews']);

        $container->parameters()->set('uxf_cms.loggable.sources', $config['loggable']['sources']);
        $container->parameters()->set('uxf_cms.cron.stage', $config['cron']['stage']);
        $container->parameters()->set('uxf_cms.cron.sources', $config['cron']['sources']);
        $container->parameters()->set('uxf_cms.cron.destinations', $config['cron']['destinations']);
        $container->parameters()->set('uxf_cms.cron.enabled_stages', $config['cron']['enabled_stages']);
    }

    public function prependExtension(ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $config = array_merge(...$builder->getExtensionConfig('uxf_cms'));

        $prod = $builder->getParameter('kernel.environment') === 'prod';
        $parameters = $container->parameters();

        // stdout
        if ($prod && (bool) ($config['logger']['stdout']['enabled'] ?? false)) {
            $parameters->set('uxf_cms.stdout.level', $config['logger']['stdout']['level'] ?? LogLevel::NOTICE);

            $container->extension('monolog', [
                'handlers' => [
                    'stdout' => [
                        'type' => 'stream',
                        'path' => 'php://stdout',
                        'level' => '%uxf_cms.stdout.level%',
                    ],
                ],
            ]);
        }

        // sentry
        $sentry = $config['logger']['sentry'] ?? [];
        $sentryEnabled = $sentry !== [];
        $parameters->set('uxf_cms.sentry.enabled', $sentryEnabled);
        $parameters->set('uxf_cms.sentry.dsn', $sentry['dsn'] ?? null);
        $parameters->set('uxf_cms.sentry.release', $sentry['release'] ?? null);
        $parameters->set('uxf_cms.sentry.environment', $sentry['environment'] ?? null);
        $parameters->set('uxf_cms.sentry.level', $sentry['level'] ?? LogLevel::WARNING);
        $parameters->set('uxf_cms.sentry.traces_sample_rate', $sentry['traces_sample_rate'] ?? 0);

        if ($sentryEnabled && $prod) {
            $container->extension('monolog', [
                'handlers' => [
                    'sentry' => [
                        'type' => 'sentry',
                        'level' => '%uxf_cms.sentry.level%',
                        'hub_id' => HubInterface::class,
                        'fill_extra_context' => true,
                    ],
                ],
            ]);

            $container->extension('sentry', [
                'dsn' => '%uxf_cms.sentry.dsn%',
                'register_error_listener' => false,
                'messenger' => [
                    'capture_soft_fails' => false,
                ],
                'options' => [
                    'traces_sample_rate' => '%uxf_cms.sentry.traces_sample_rate%',
                    'environment' => '%uxf_cms.sentry.environment%',
                    'release' => '%uxf_cms.sentry.release%',
                    'send_default_pii' => true,
                ],
                'tracing' => [
                    'cache' => [
                        'enabled' => false,
                    ],
                    'twig' => [
                        'enabled' => false,
                    ],
                ],
            ]);
        }
    }

    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new AutocompleteTypePass());
        $container->addCompilerPass(new ErrorListenerCompilerPass());
        $container->addCompilerPass(new ChangeLogCompilerPass());
    }
}
