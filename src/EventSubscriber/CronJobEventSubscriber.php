<?php

declare(strict_types=1);

namespace UXF\CMS\EventSubscriber;

use ReflectionClass;
use Sentry\CheckInStatus;
use Sentry\MonitorConfig;
use Sentry\MonitorSchedule;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\Service\ResetInterface;
use UXF\CMS\Attribute\CronJob;
use function Sentry\captureCheckIn;

final class CronJobEventSubscriber implements EventSubscriberInterface, ResetInterface
{
    private ?string $checkInID = null;
    private ?string $slug = null;

    /**
     * @param string[]|null $enabledStages
     */
    public function __construct(
        private readonly bool $debug,
        private readonly ?string $appStage,
        private readonly ?array $enabledStages,
    ) {
    }

    public function onStart(ConsoleCommandEvent $event): void
    {
        $cmd = $event->getCommand();
        if ($cmd === null) {
            return;
        }

        $attr = (new ReflectionClass($cmd))->getAttributes(CronJob::class)[0] ?? null;
        if ($attr === null) {
            return;
        }

        $cronJob = $attr->newInstance();
        $enabledStages = $cronJob->enabledStages ?? $this->enabledStages;
        $appStage = $this->appStage;

        // run cron only on allowed stages
        if (!$this->debug && isset($appStage, $enabledStages) && !in_array($appStage, $enabledStages, true)) {
            $event->disableCommand();
            return;
        }

        $slug = $cronJob->slug ?? $cmd->getName() ?? 'unknown';
        $this->slug = $slug = str_replace(':', '_', $slug);

        $this->checkInID = captureCheckIn(
            slug: $slug,
            status: CheckInStatus::inProgress(),
            monitorConfig: new MonitorConfig(
                schedule: MonitorSchedule::crontab($cronJob->crontab),
                maxRuntime: $cronJob->sentryMaxRuntime,
                timezone: date_default_timezone_get(),
            ),
        );
    }

    public function onFinish(ConsoleTerminateEvent $event): void
    {
        if ($this->slug === null) {
            return;
        }

        captureCheckIn(
            slug: $this->slug,
            status: $event->getExitCode() === 0 ? CheckInStatus::ok() : CheckInStatus::error(),
            checkInId: $this->checkInID,
        );
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ConsoleCommandEvent::class => 'onStart',
            ConsoleTerminateEvent::class => 'onFinish',
        ];
    }

    public function reset(): void
    {
        $this->checkInID = null;
        $this->slug = null;
    }
}
