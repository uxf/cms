<?php

declare(strict_types=1);

namespace UXF\CMS\EventSubscriber;

use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\Mailer\Event\MessageEvent;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Header\UnstructuredHeader;

#[AsEventListener(MessageEvent::class)]
final readonly class MailerTagListener
{
    public function __construct(
        private string $tag,
    ) {
    }

    public function __invoke(MessageEvent $event): void
    {
        $message = $event->getMessage();
        if ($this->tag === '' || !$message instanceof Email) {
            return;
        }

        $message->getHeaders()->add(new UnstructuredHeader('X-Tags', $this->tag));
    }
}
