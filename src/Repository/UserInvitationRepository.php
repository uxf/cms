<?php

declare(strict_types=1);

namespace UXF\CMS\Repository;

use Doctrine\ORM\EntityManagerInterface;
use UXF\CMS\Entity\Invitation\UserInvitation;

final readonly class UserInvitationRepository
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function findByToken(string $token): ?UserInvitation
    {
        return $this->entityManager->getRepository(UserInvitation::class)->findOneBy([
            'key' => $token,
        ]);
    }
}
