<?php

declare(strict_types=1);

namespace UXF\CMS\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use UXF\CMS\Entity\User;
use UXF\Core\Exception\BasicException;
use UXF\Core\Type\Email;
use UXF\Security\Service\AuthUserProvider;

/**
 * @implements AuthUserProvider<User>
 */
final readonly class UserRepository implements AuthUserProvider
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function find(int $id): ?User
    {
        return $this->entityManager->find(User::class, $id);
    }

    public function findByEmail(Email $email): ?User
    {
        return $this->findByUsername($email->toString());
    }

    public function findByUsername(string $username): ?User
    {
        return $this->entityManager->getRepository(User::class)->findOneBy([
            'email' => mb_strtolower($username),
        ]);
    }

    public function check(UserInterface $user, bool $refresh): void
    {
        if (!$user->isActive()) {
            throw $refresh
                ? BasicException::unauthorized('User is not active')
                : BasicException::badRequest('User is not active');
        }

        if (!$refresh) {
            $user->setLastLoginAt();
            $this->entityManager->flush();
        }
    }
}
