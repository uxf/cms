<?php

declare(strict_types=1);

namespace UXF\CMS\Repository;

use Doctrine\ORM\EntityManagerInterface;
use UXF\CMS\Entity\User;
use UXF\CMS\Entity\UserConfig;
use UXF\Core\Exception\BasicException;

final readonly class UserConfigRepository
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function findByName(User $user, string $name): ?UserConfig
    {
        return $this->entityManager->getRepository(UserConfig::class)->findOneBy([
            'user' => $user,
            'name' => $name,
        ]);
    }

    public function get(User $user, string $name): UserConfig
    {
        return $this->findByName($user, $name) ?? throw BasicException::notFound('User Config not found.');
    }

    /**
     * @return array<string, UserConfig>
     */
    public function getAllByUser(User $user): array
    {
        return $this->entityManager->createQueryBuilder()
            ->select('c')
            ->from(UserConfig::class, 'c', 'c.name')
            ->where('c.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
