<?php

declare(strict_types=1);

namespace UXF\CMSTests\Project\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UXF\CMS\Attribute\CronJob;

#[CronJob('1 2 3 4 5')]
#[AsCommand('fake:cron')]
class FakeCronCommand extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        return 1;
    }
}
