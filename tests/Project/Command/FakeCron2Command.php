<?php

declare(strict_types=1);

namespace UXF\CMSTests\Project\Command;

use LogicException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UXF\CMS\Attribute\CronJob;

#[CronJob('*/10 2 3 4 5', slug: 'X', args: '--debug', enabledStages: ['test'])]
#[AsCommand('fake:cron2')]
class FakeCron2Command extends Command
{
    protected function configure(): void
    {
        $this->addOption('fail');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($input->getOption('fail') === true) {
            throw new LogicException();
        }

        return 0;
    }
}
