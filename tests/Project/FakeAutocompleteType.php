<?php

declare(strict_types=1);

namespace UXF\CMSTests\Project;

use Doctrine\ORM\EntityManagerInterface;
use UXF\CMS\Autocomplete\Adapter\DoctrineAutocomplete;
use UXF\CMS\Autocomplete\AutocompleteBuilder;
use UXF\CMS\Autocomplete\AutocompleteType;
use UXF\CMS\Entity\Role;

final readonly class FakeAutocompleteType implements AutocompleteType
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /**
     * @inheritdoc
     */
    public function buildAutocomplete(AutocompleteBuilder $builder, array $options = []): void
    {
        $autocomplete = DoctrineAutocomplete::create($this->entityManager, Role::class, ['name'], '{id} {name} {title}!');
        $autocomplete->qb->andWhere('e.id > 1');
        $builder->setAutocomplete($autocomplete);
    }

    public static function getName(): string
    {
        return 'fake';
    }
}
