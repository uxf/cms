<?php

declare(strict_types=1);

namespace UXF\CMSTests\Project;

enum UserRole: string
{
    case PUBLIC = 'PUBLIC';
    case LOGGED = 'LOGGED';
    case ROLE_ROOT = 'ROLE_ROOT';
}
