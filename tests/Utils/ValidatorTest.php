<?php

declare(strict_types=1);

namespace UXF\CMSTests\Utils;

use PHPUnit\Framework\TestCase;
use UXF\CMS\Utils\Validator;

class ValidatorTest extends TestCase
{
    public function testIsEmail(): void
    {
        self::assertTrue(Validator::isEmail('aa@bb.com'));
        self::assertTrue(Validator::isEmail('Aa@Bb.com'));
        self::assertFalse(Validator::isEmail('aabb.com'));
        self::assertFalse(Validator::isEmail('aa@bbcom'));
        self::assertFalse(Validator::isEmail('aabbcom'));
    }
}
