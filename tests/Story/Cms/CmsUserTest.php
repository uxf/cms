<?php

declare(strict_types=1);

namespace UXF\CMSTests\Story\Cms;

use Doctrine\ORM\EntityManagerInterface;
use UXF\CMS\Entity\Role;
use UXF\CMS\Entity\User;
use UXF\CMS\Repository\RoleRepository;
use UXF\CMS\Service\UserService;
use UXF\CMSTests\Story\StoryTestCase;
use UXF\Core\Type\Email;

class CmsUserTest extends StoryTestCase
{
    public function testHierarchy(): void
    {
        $client = self::createClient();

        $client->login();

        $client->get('/api/cms/user');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());
    }

    public function testMe(): void
    {
        $client = self::createClient();

        $client->login();

        $client->get('/api/cms/me');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());
    }

    public function testMeUnauthorized(): void
    {
        $client = self::createClient();

        $client->get('/api/cms/me');
        self::assertResponseStatusCodeSame(401);
        $this->assertSnapshot($client->getResponseData());
    }

    public function testMeForbidden(): void
    {
        $client = self::createClient();

        $client->login('admin@uxf.cz', 'admin');

        $client->get('/api/cms/me');
        self::assertResponseStatusCodeSame(403);
        $this->assertSnapshot($client->getResponseData());
    }

    public function testAddAndRemoveRole(): void
    {
        self::bootKernel();

        $container = self::getContainer();

        $entityManager = $container->get(EntityManagerInterface::class);
        assert($entityManager instanceof EntityManagerInterface);

        $roleRepository = $container->get(RoleRepository::class);
        assert($roleRepository instanceof RoleRepository);
        $adminRole = $roleRepository->get(Role::ROLE_ADMIN);

        $service = $container->get(UserService::class);
        assert($service instanceof UserService);

        $user = new User(Email::of('x@uxf.cz'), 'X');
        $entityManager->persist($user);

        // test
        $service->addRole($user, $adminRole->getName());
        self::assertCount(1, $user->getUserRoles());
        self::assertSame($adminRole, $user->getUserRoles()->first());

        $service->removeRole($user, $adminRole->getName());
        self::assertTrue($user->getUserRoles()->isEmpty());
    }
}
