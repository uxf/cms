<?php

declare(strict_types=1);

namespace UXF\CMSTests\Story\Loggable;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use UXF\CMS\Entity\ChangeLog;
use UXF\CMSTests\Entity\Article;
use UXF\CMSTests\Entity\Status;
use UXF\CMSTests\Entity\Tag;
use UXF\CMSTests\Story\StoryTestCase;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;

class LoggableStoryTest extends StoryTestCase
{
    public function test(): void
    {
        $client = self::createClient();
        $this->prepareData();

        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        $tag = new Tag('foo');
        $entityManager->persist($tag);

        $article = $entityManager->find(Article::class, 1) ?? throw new Exception('Article not found');
        $article->setDate(new Date('2020-01-02'));
        $article->setDatetime(new DateTime('2020-02-03 10:00'));
        $article->setTime(new Time('12:00'));
        $article->getInfo()->setInteger(15);
        $article->getOneToOneFieldset()->setManyToOne($tag);
        $article->setInteger(5);
        $article->setFloat(1.1);
        $article->setStatus(Status::OLD);
        $article->setString('fooo');
        $article->setText('long text');
        $article->setPhone(Phone::of('+420774993333'));
        $entityManager->flush();


        $changeLogs = $entityManager->getRepository(ChangeLog::class)->findAll();
        $changeValues = [];
        foreach ($changeLogs as $changeLog) {
            $changeValues[] = $changeLog->getValues();
        }
        $this->assertSnapshot($changeValues);
    }

    public function testUnchanged(): void
    {
        $client = self::createClient();
        $this->prepareData();

        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        $tag = new Tag('foo');
        $entityManager->persist($tag);

        $article = $entityManager->find(Article::class, 1) ?? throw new Exception('Article not found');
        $article->setDate(new Date('2020-01-01'));
        $article->setDatetime(new DateTime('2020-02-02 10:00'));
        $article->setTime(new Time('10:00'));
        $article->getInfo()->setInteger(1000);
        $article->setInteger(0);
        $article->setFloat(0.0);
        $article->setStatus(Status::NEW);
        $article->setString('');
        $article->setText('');
        $article->setPhone(Phone::of('+420774993336'));
        $entityManager->flush();


        $changeLogs = $entityManager->getRepository(ChangeLog::class)->findAll();
        self::assertEquals([], $changeLogs);
    }

    private function prepareData(): void
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        $tag = new Tag('first');
        $entityManager->persist($tag);
        // prepareData
        $article = new Article($tag);
        $entityManager->persist($article);
        $entityManager->flush();
        $entityManager->clear();
    }
}
