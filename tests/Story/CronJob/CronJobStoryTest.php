<?php

declare(strict_types=1);

namespace UXF\CMSTests\Story\CronJob;

use Exception;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Tester\CommandTester;
use UXF\CMSTests\Kernel;
use UXF\CMSTests\Story\StoryTestCase;

class CronJobStoryTest extends StoryTestCase
{
    public function testGen(): void
    {
        if (file_exists(__DIR__ . '/../../generated/cron')) {
            unlink(__DIR__ . '/../../generated/cron');
        }

        self::bootKernel();
        $application = new Application(self::$kernel ?? throw new Exception('Missing Kernel'));
        $command = $application->find('uxf:cron-gen');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);
        $commandTester->assertCommandIsSuccessful();

        self::assertFileEquals(__DIR__ . '/../../expected/cron', __DIR__ . '/../../generated/cron');
    }

    public function testSkip(): void
    {
        $application = new Application(new Kernel('test', false));
        $application->setAutoExit(false);
        $application->setCatchExceptions(false);
        $exit = $application->run(new StringInput('fake:cron'), new NullOutput());
        self::assertSame(ConsoleCommandEvent::RETURN_CODE_DISABLED, $exit);
    }

    public function testDebug(): void
    {
        $application = new Application(new Kernel('test', true));
        $application->setAutoExit(false);
        $application->setCatchExceptions(false);
        $exit = $application->run(new StringInput('fake:cron'), new NullOutput());
        self::assertSame(1, $exit);
    }

    public function testRun(): void
    {
        $application = new Application(new Kernel('test', false));
        $application->setAutoExit(false);
        $application->setCatchExceptions(false);
        $exit = $application->run(new StringInput('fake:cron2'), new NullOutput());
        self::assertSame(0, $exit);
    }

    public function testException(): void
    {
        $application = new Application(new Kernel('test', false));
        $application->setAutoExit(false);
        $exit = $application->run(new StringInput('fake:cron2 --fail'), new NullOutput());
        self::assertSame(1, $exit);
    }
}
