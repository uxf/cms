<?php

declare(strict_types=1);

namespace UXF\CMSTests\Story\Content;

use UXF\CMSTests\Story\StoryTestCase;

class TagStoryTest extends StoryTestCase
{
    public function test(): void
    {
        $client = self::createClient();
        $client->post('/api/cms/form/content-tag', []);
        self::assertResponseStatusCodeSame(401);

        $client->login();

        $client->post('/api/cms/form/content-tag', [
            "code" => "kod",
            "label" => "Label...",
        ]);

        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->get('/api/cms/form/content-tag/1');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->get('/api/cms/datagrid/content-tag?sort=code&dir=asc&perPage=10&page=0');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->get('/api/cms/autocomplete/content-tag?' . http_build_query([
            'term' => 'LÁB',
        ]));
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());
    }
}
