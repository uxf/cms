<?php

declare(strict_types=1);

namespace UXF\CMSTests\Story\Content;

use UXF\CMSTests\Story\StoryTestCase;

class ContentStoryTest extends StoryTestCase
{
    public function test(): void
    {
        $client = self::createClient();
        $client->post('/api/cms/content', []);
        self::assertResponseStatusCodeSame(401);

        $client->login();

        $client->post('/api/cms/form/content-author', [
            'firstName' => 'Tom',
            'surname' => 'Jerry',
        ]);

        $client->post('/api/cms/form/content-tag', [
            'code' => 'X',
            'label' => 'Hello',
        ]);

        $client->post('/api/cms/form/content-category', [
            'name' => 'Magic',
        ]);

        $client->post('/api/cms/content', [
            'type' => 'TYPE',
            'name' => 'NAME',
            'perex' => 'PEREX',
            'seo' => [
                'name' => 'N',
                'title' => 'T',
                'description' => 'D',
                'ogTitle' => 'OT',
                'ogDescription' => 'OD',
                'ogImage' => null,
            ],
            'visibilityLevel' => 'PUBLIC',
            'hidden' => false,
            'image' => null,
            'publishedAt' => '2020-01-01T17:21:10+01:00',
            'author' => 1,
            'parent' => null,
            'category' => 1,
            'content' => [
                'data' => [[
                    'type' => 'wysiwyg',
                    'content' => [
                        'property' => 'value',
                    ],
                ]],
                'search' => '',
            ],
            'tags' => [1],
        ]);
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->get('/api/cms/content/1');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->get('/api/cms/datagrid/content');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->put('/api/cms/content/1', [
            // required, but ignored
            'type' => 'TYPE',
            'name' => 'NAME_2',
            'perex' => 'PEREX_2',
            'seo' => [
                'name' => 'N_2',
                'title' => 'T_2',
                'description' => 'D_2',
                'ogTitle' => 'OT_2',
                'ogDescription' => 'OD_2',
                'ogImage' => null,
            ],
            'visibilityLevel' => 'PUBLIC_WITHOUT_SITEMAP',
            'hidden' => true,
            'image' => null,
            'publishedAt' => '2022-01-01T17:21:10+01:00',
            'author' => null,
            'parent' => 1,
            'category' => null,
            'content' => [
                'data' => [[
                    'type' => 'wysiwyg',
                    'content' => [
                        'property' => 'value_2',
                    ],
                ]],
                'search' => '',
            ],
            'tags' => [],
        ]);
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());
    }
}
