<?php

declare(strict_types=1);

namespace UXF\CMSTests\Story\Security;

use UXF\CMSTests\Story\StoryTestCase;

class SecurityTest extends StoryTestCase
{
    public function test(): void
    {
        $client = self::createClient();
        $client->get('/security');
        self::assertResponseStatusCodeSame(401);

        $client->login('admin@uxf.cz', 'admin');
        $client->get('/security');
        self::assertResponseStatusCodeSame(403);

        $client->login();
        $client->get('/security');
        self::assertResponseIsSuccessful();
    }
}
