<?php

declare(strict_types=1);

namespace UXF\CMSTests\Story\Autocomplete;

use UXF\CMSTests\Story\StoryTestCase;

class AutocompleteTest extends StoryTestCase
{
    public function test(): void
    {
        $client = self::createClient();
        $client->get('/api/cms/autocomplete/fake');
        self::assertResponseStatusCodeSame(401);

        $client->login();

        $client->get('/api/cms/autocomplete/fake?term=');

        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());
    }
}
