<?php

declare(strict_types=1);

namespace UXF\CMSTests\Story\FormMapper;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use UXF\CMSTests\Story\StoryTestCase;
use UXF\Core\SystemProvider\Uuid;
use UXF\Core\Test\Client;

class FormMapperStoryTest extends StoryTestCase
{
    public function test(): void
    {
        $client = self::createClient();
        $client->login();
        $this->uploadFile($client);
        $this->uploadFile($client);

        $client->get('/api/cms/form/form-entity/schema');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->post('/api/cms/form/form-entity', [
            'email' => 'info@uxf.cz',
            'phone' => '+420731666999',
            'ban' => '123/0300',
            'nin' => '930201/3545',
            'dateTime' => '2020-01-01T17:21:10+01:00',
            'date' => '2020-01-01',
            'time' => '17:21:10',
            'decimal' => '3.14',
            'url' => 'https://www.appio.dev',
            'country' => 'CZ',
            'file' => [
                'id' => 1,
            ],
            'image' => [
                'id' => 1,
            ],
            'files' => [[
                'id' => 1,
            ]],
            'images' => [[
                'id' => 1,
            ]],
        ]);
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->put('/api/cms/form/form-entity/1', [
            'email' => 'dev@uxf.cz',
            'phone' => '+420731666888',
            'ban' => '1230/0300',
            'nin' => '740718/9900',
            'dateTime' => '2020-01-02T17:21:10+01:00',
            'date' => '2020-01-02',
            'time' => '17:21:11',
            'decimal' => '3.141',
            'url' => 'https://www.uxf.cz',
            'country' => 'SK',
            'file' => [
                'id' => 2,
            ],
            'image' => [
                'id' => 2,
            ],
            'files' => [[
                'id' => 2,
            ]],
            'images' => [],
        ]);
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());
    }

    private function uploadFile(Client $client): void
    {
        // create file copy for upload testing
        $uploadedFile = __DIR__ . '/../../files/file.png';
        $tmpFile = '/tmp/tmp_file.png';
        copy($uploadedFile, $tmpFile);

        $client->request('POST', '/api/cms/files/upload', files: [
            'files' => [new UploadedFile($tmpFile, 'fileX.png', 'image/png')],
        ]);
    }

    protected function setUp(): void
    {
        Uuid::$seq = 1;
        parent::setUp();
    }

    protected function tearDown(): void
    {
        Uuid::$seq = null;
        parent::tearDown();
    }
}
