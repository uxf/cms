<?php

declare(strict_types=1);

namespace UXF\CMSTests;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\CMS\UXFCmsBundle;
use UXF\Content\UXFContentBundle;
use UXF\Core\UXFCoreBundle;
use UXF\DataGrid\UXFDataGridBundle;
use UXF\Form\UXFFormBundle;
use UXF\Hydrator\UXFHydratorBundle;
use UXF\Security\UXFSecurityBundle;
use UXF\Storage\UXFStorageBundle;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    /**
     * @inheritDoc
     */
    public function registerBundles(): iterable
    {
        $bundles = [
            FrameworkBundle::class,
            SecurityBundle::class,
            TwigBundle::class,
            DoctrineBundle::class,
            DoctrineFixturesBundle::class,
            MonologBundle::class,
            UXFCoreBundle::class,
            UXFSecurityBundle::class,
            UXFCmsBundle::class,
            UXFContentBundle::class,
            UXFDataGridBundle::class,
            UXFFormBundle::class,
            UXFStorageBundle::class,
            UXFHydratorBundle::class,
        ];

        foreach ($bundles as $bundle) {
            yield new $bundle();
        }
    }

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->import(__DIR__ . '/config/services.php');
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import('@UXFSecurityBundle/config/routes.php');
        $routes->import('@UXFContentBundle/config/routes.php');
        $routes->import('@UXFDataGridBundle/config/routes.php');
        $routes->import('@UXFFormBundle/config/routes.php');
        $routes->import(__DIR__ . '/../config/routes.php');
        $routes->import(__DIR__ . '/config/routes.php');
    }
}
