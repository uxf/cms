<?php

declare(strict_types=1);

namespace UXF\CMSTests\Email;

readonly class TemplateDto
{
    public function __construct(
        public string $link,
        public string $title,
    ) {
    }
}
