<?php

declare(strict_types=1);

namespace UXF\CMSTests\Email;

use Override;
use UXF\CMS\Mime\PreviewableEmail;

final class SampleEmail extends PreviewableEmail
{
    /**
     * @param TemplateDto[] $templates
     */
    public function __construct(
        public readonly array $templates,
    ) {
        parent::__construct('@UXFCms/email-templates.twig');
    }

    /**
     * @return iterable<string, self>
     */
    #[Override]
    public static function getPreviewData(): iterable
    {
        return [
            'default' => new self([
                new TemplateDto('https://uxf.dev', 'Uxf'),
            ]),
        ];
    }
}
