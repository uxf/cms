<?php

declare(strict_types=1);

namespace UXF\CMSTests\Entity;

use Doctrine\ORM\Mapping as ORM;
use UXF\CMS\Attribute\CmsForm;
use UXF\CMS\Attribute\CmsMeta;

#[ORM\Entity]
#[CmsMeta(alias: 'test-article-uniq-info', title: 'Article')]
class ArticleUniqInfo
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue]
    private int $id = 0;

    #[ORM\Column]
    #[CmsForm(label: 'NAME')]
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
