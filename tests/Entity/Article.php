<?php

declare(strict_types=1);

namespace UXF\CMSTests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use UXF\CMS\Attribute\CmsForm;
use UXF\CMS\Attribute\CmsGrid;
use UXF\CMS\Attribute\CmsMeta;
use UXF\CMS\Attribute\Loggable;
use UXF\Content\Entity\ContentLite;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Storage\Entity\File;
use UXF\Storage\Entity\Image;

#[ORM\Entity]
#[CmsMeta(alias: 'test-article', title: 'Article')]
class Article
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue]
    private ?int $id = 0;

    #[Loggable]
    #[ORM\Column]
    #[CmsForm(label: 'INTEGER')]
    private int $integer = 0;

    #[Loggable]
    #[ORM\Column]
    #[CmsForm(label: 'FLOAT')]
    private float $float = 0.0;

    #[Loggable]
    #[ORM\Column]
    #[CmsForm(label: 'STRING')]
    private string $string = '';

    #[Loggable]
    #[ORM\Column(type: 'text')]
    #[CmsForm(label: 'TEXT')]
    private string $text = '';

    #[Loggable]
    #[ORM\Column(type: Date::class)]
    #[CmsForm(label: 'DATE')]
    private Date $date;

    #[Loggable]
    #[ORM\Column(type: DateTime::class)]
    #[CmsForm(label: 'DATETIME')]
    private DateTime $datetime;

    #[Loggable]
    #[ORM\Column(type: Time::class)]
    #[CmsForm(label: 'TIME')]
    private Time $time;

    #[Loggable]
    #[ORM\Column(type: Phone::class)]
    #[CmsForm(label: 'PHONE')]
    private Phone $phone;

    #[Loggable]
    #[ORM\ManyToOne, ORM\JoinColumn(nullable: false)]
    #[CmsForm(label: 'MANY TO ONE', targetField: 'name', autocomplete: 'tag')]
    private Tag $manyToOne;

    /**
     * @var Collection<int, Tag>
     */
    #[ORM\ManyToMany(targetEntity: Tag::class)]
    #[CmsForm(label: 'MANY TO MANY', targetField: 'name', autocomplete: 'tag')]
    private Collection $manyToMany;

    /**
     * @var Collection<int, Comment>
     */
    #[ORM\OneToMany(mappedBy: 'article', targetEntity: Comment::class, indexBy: 'id')]
    #[CmsForm(label: 'ONE TO MANY')]
    private Collection $oneToMany;

    #[ORM\OneToOne]
    #[CmsForm(label: 'ARTICLE UNIQ INFO', targetField: 'name', autocomplete: 'tag')]
    private ?ArticleUniqInfo $oneToOne = null;

    #[CmsForm(label: 'ARTICLE FIELDSET INFO', fieldset: true)]
    #[ORM\OneToOne(cascade: ['persist']), ORM\JoinColumn(nullable: false)]
    private ArticleFieldsetInfo $oneToOneFieldset;

    #[Loggable]
    #[ORM\Embedded]
    #[CmsForm(label: 'INFO')]
    private Info $info;

    #[Loggable]
    #[ORM\Column]
    #[CmsForm(label: 'STATUS')]
    #[CmsGrid]
    private Status $status = Status::NEW;

    #[ORM\ManyToOne]
    #[CmsForm(label: 'FILE')]
    private ?File $file = null;

    #[ORM\ManyToOne]
    #[CmsForm(label: 'IMAGE')]
    private ?Image $image = null;

    #[ORM\Embedded]
    #[CmsForm(label: 'CONTENT')]
    private ContentLite $content;

    public function __construct(Tag $manyToOne)
    {
        $this->date = new Date('2020-01-01');
        $this->datetime = new DateTime('2020-02-02 10:00');
        $this->time = new Time('10:00');
        $this->phone = Phone::of('+420774993336');
        $this->manyToOne = $manyToOne;
        $this->manyToMany = new ArrayCollection();
        $this->oneToMany = new ArrayCollection();
        $this->oneToOneFieldset = new ArticleFieldsetInfo($manyToOne);
        $this->info = new Info();
        $this->content = new ContentLite([], '');
    }

    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function getInteger(): int
    {
        return $this->integer;
    }

    public function setInteger(int $integer): void
    {
        $this->integer = $integer;
    }

    public function getFloat(): float
    {
        return $this->float;
    }

    public function setFloat(float $float): void
    {
        $this->float = $float;
    }

    public function getString(): string
    {
        return $this->string;
    }

    public function setString(string $string): void
    {
        $this->string = $string;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function getDate(): Date
    {
        return $this->date;
    }

    public function setDate(Date $date): void
    {
        $this->date = $date;
    }

    public function getDatetime(): DateTime
    {
        return $this->datetime;
    }

    public function setDatetime(DateTime $datetime): void
    {
        $this->datetime = $datetime;
    }

    public function getTime(): Time
    {
        return $this->time;
    }

    public function setTime(Time $time): void
    {
        $this->time = $time;
    }

    public function getManyToOne(): Tag
    {
        return $this->manyToOne;
    }

    public function setManyToOne(Tag $manyToOne): void
    {
        $this->manyToOne = $manyToOne;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getManyToMany(): Collection
    {
        return $this->manyToMany;
    }

    /**
     * @param Collection<int, Tag> $manyToMany
     */
    public function setManyToMany(Collection $manyToMany): void
    {
        $this->manyToMany = $manyToMany;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getOneToMany(): Collection
    {
        return $this->oneToMany;
    }

    /**
     * @param Collection<int, Comment> $oneToMany
     */
    public function setOneToMany(Collection $oneToMany): void
    {
        $this->oneToMany = $oneToMany;
    }

    public function getOneToOne(): ?ArticleUniqInfo
    {
        return $this->oneToOne;
    }

    public function setOneToOne(?ArticleUniqInfo $oneToOne): void
    {
        $this->oneToOne = $oneToOne;
    }

    public function getOneToOneFieldset(): ArticleFieldsetInfo
    {
        return $this->oneToOneFieldset;
    }

    public function setOneToOneFieldset(ArticleFieldsetInfo $oneToOneFieldset): void
    {
        $this->oneToOneFieldset = $oneToOneFieldset;
    }

    public function getInfo(): Info
    {
        return $this->info;
    }

    public function setInfo(Info $info): void
    {
        $this->info = $info;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): void
    {
        $this->status = $status;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): void
    {
        $this->file = $file;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): void
    {
        $this->image = $image;
    }

    public function getContent(): ContentLite
    {
        return $this->content;
    }

    public function setContent(ContentLite $content): void
    {
        $this->content = $content;
    }

    public function getPhone(): Phone
    {
        return $this->phone;
    }

    public function setPhone(Phone $phone): Article
    {
        $this->phone = $phone;
        return $this;
    }
}
