<?php

declare(strict_types=1);

namespace UXF\CMSTests\Entity;

use Doctrine\ORM\Mapping as ORM;
use Override;
use UXF\CMS\Attribute\CmsForm;
use UXF\CMS\Attribute\CmsMeta;
use UXF\CMS\Contract\NormalizedLoggable;

#[ORM\Entity]
#[CmsMeta(alias: 'test-tag', title: 'Article')]
class Tag implements NormalizedLoggable
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue]
    private int $id = 0;

    #[ORM\Column]
    #[CmsForm(label: 'NAME')]
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /** @return array<string, mixed> */
    #[Override] public function normalize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
