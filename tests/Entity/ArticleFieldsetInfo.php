<?php

declare(strict_types=1);

namespace UXF\CMSTests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Override;
use UXF\CMS\Attribute\CmsForm;
use UXF\CMS\Attribute\CmsMeta;
use UXF\CMS\Attribute\Loggable;
use UXF\CMS\Contract\NormalizedLoggable;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Time;

#[ORM\Entity]
#[CmsMeta(alias: 'test-article-fieldset-info', title: 'Article')]
class ArticleFieldsetInfo implements NormalizedLoggable
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue]
    private int $id = 0;

    #[ORM\Column]
    #[CmsForm(label: 'INTEGER?')]
    private int $integer = 0;

    #[ORM\Column]
    #[CmsForm(label: 'STRING?')]
    private string $string = '';

    #[ORM\Column(type: 'text')]
    #[CmsForm(label: 'TEXT?')]
    private string $text = '';

    #[ORM\Column(type: Date::class)]
    #[CmsForm(label: 'DATE?')]
    private Date $date;

    #[ORM\Column(type: DateTime::class)]
    #[CmsForm(label: 'DATETIME?')]
    private DateTime $datetime;

    #[ORM\Column(type: Time::class)]
    #[CmsForm(label: 'TIME?')]
    private Time $time;

    #[Loggable]
    #[ORM\ManyToOne, ORM\JoinColumn(nullable: false)]
    #[CmsForm(label: 'MANY TO ONE?', targetField: 'name', autocomplete: 'tag')]
    private Tag $manyToOne;

    /**
     * @var Collection<int, Tag>
     */
    #[ORM\ManyToMany(targetEntity: Tag::class)]
    #[CmsForm(label: 'MANY TO MANY?', targetField: 'name', autocomplete: 'tag')]
    private Collection $manyToMany;

    public function __construct(Tag $manyToOne)
    {
        $this->date = new Date('2020-01-01');
        $this->datetime = new DateTime('2020-02-02 10:00');
        $this->time = new Time('10:00');
        $this->manyToOne = $manyToOne;
        $this->manyToMany = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getInteger(): int
    {
        return $this->integer;
    }

    public function setInteger(int $integer): void
    {
        $this->integer = $integer;
    }

    public function getString(): string
    {
        return $this->string;
    }

    public function setString(string $string): void
    {
        $this->string = $string;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function getDate(): Date
    {
        return $this->date;
    }

    public function setDate(Date $date): void
    {
        $this->date = $date;
    }

    public function getDatetime(): DateTime
    {
        return $this->datetime;
    }

    public function getTime(): Time
    {
        return $this->time;
    }

    public function setTime(Time $time): void
    {
        $this->time = $time;
    }

    public function setDatetime(DateTime $datetime): void
    {
        $this->datetime = $datetime;
    }

    public function getManyToOne(): Tag
    {
        return $this->manyToOne;
    }

    public function setManyToOne(Tag $manyToOne): void
    {
        $this->manyToOne = $manyToOne;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getManyToMany(): Collection
    {
        return $this->manyToMany;
    }

    /**
     * @param Collection<int, Tag> $manyToMany
     */
    public function setManyToMany(Collection $manyToMany): void
    {
        $this->manyToMany = $manyToMany;
    }

    /** @return array<string, mixed> */
    #[Override] public function normalize(): array
    {
        return [
            'id' => $this->id,
            'manyToOne' => $this->manyToOne,
        ];
    }
}
