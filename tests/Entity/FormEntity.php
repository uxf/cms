<?php

declare(strict_types=1);

namespace UXF\CMSTests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ReadableCollection;
use Doctrine\ORM\Mapping as ORM;
use UXF\CMS\Attribute\CmsForm;
use UXF\CMS\Attribute\CmsMeta;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Country;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\Storage\Entity\File;
use UXF\Storage\Entity\Image;

#[ORM\Entity]
#[CmsMeta(alias: 'form-entity', title: 'TEST')]
class FormEntity
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id = 0;

    #[CmsForm]
    #[ORM\Column(type: Email::class, nullable: true)]
    private ?Email $email = null;

    #[CmsForm]
    #[ORM\Column(type: Phone::class, nullable: true)]
    private ?Phone $phone = null;

    #[CmsForm]
    #[ORM\Column(type: BankAccountNumberCze::class, nullable: true)]
    private ?BankAccountNumberCze $ban = null;

    #[CmsForm]
    #[ORM\Column(type: NationalIdentificationNumberCze::class, nullable: true)]
    private ?NationalIdentificationNumberCze $nin = null;

    #[CmsForm]
    #[ORM\Column(type: DateTime::class, nullable: true)]
    private ?DateTime $dateTime = null;

    #[CmsForm]
    #[ORM\Column(type: Date::class, nullable: true)]
    private ?Date $date = null;

    #[CmsForm]
    #[ORM\Column(type: Time::class, nullable: true)]
    private ?Time $time = null;

    #[CmsForm]
    #[ORM\Column(type: Decimal::class, nullable: true)]
    private ?Decimal $decimal = null;

    #[CmsForm]
    #[ORM\Column(type: Url::class, nullable: true)]
    private ?Url $url = null;

    #[CmsForm]
    #[ORM\Column(nullable: true)]
    private ?Country $country = null;

    #[CmsForm]
    #[ORM\ManyToOne]
    private ?File $file = null;

    #[CmsForm]
    #[ORM\ManyToOne]
    private ?Image $image = null;

    /** @var Collection<int, File> */
    #[CmsForm]
    #[ORM\ManyToMany(targetEntity: File::class)]
    private Collection $files;

    /** @var Collection<int, Image> */
    #[CmsForm]
    #[ORM\ManyToMany(targetEntity: Image::class)]
    private Collection $images;

    public function __construct()
    {
        $this->files = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): ?Email
    {
        return $this->email;
    }

    public function setEmail(?Email $email): void
    {
        $this->email = $email;
    }

    public function getPhone(): ?Phone
    {
        return $this->phone;
    }

    public function setPhone(?Phone $phone): void
    {
        $this->phone = $phone;
    }

    public function getBan(): ?BankAccountNumberCze
    {
        return $this->ban;
    }

    public function setBan(?BankAccountNumberCze $ban): void
    {
        $this->ban = $ban;
    }

    public function getNin(): ?NationalIdentificationNumberCze
    {
        return $this->nin;
    }

    public function setNin(?NationalIdentificationNumberCze $nin): void
    {
        $this->nin = $nin;
    }

    public function getDateTime(): ?DateTime
    {
        return $this->dateTime;
    }

    public function setDateTime(?DateTime $dateTime): void
    {
        $this->dateTime = $dateTime;
    }

    public function getDate(): ?Date
    {
        return $this->date;
    }

    public function setDate(?Date $date): void
    {
        $this->date = $date;
    }

    public function getTime(): ?Time
    {
        return $this->time;
    }

    public function setTime(?Time $time): void
    {
        $this->time = $time;
    }

    public function getDecimal(): ?Decimal
    {
        return $this->decimal;
    }

    public function setDecimal(?Decimal $decimal): void
    {
        $this->decimal = $decimal;
    }

    public function getUrl(): ?Url
    {
        return $this->url;
    }

    public function setUrl(?Url $url): void
    {
        $this->url = $url;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): void
    {
        $this->country = $country;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): void
    {
        $this->file = $file;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): void
    {
        $this->image = $image;
    }

    /**
     * @return ReadableCollection<int, File>
     */
    public function getFiles(): ReadableCollection
    {
        return $this->files;
    }

    /**
     * @param Collection<int, File> $files
     */
    public function setFiles(Collection $files): void
    {
        $this->files = $files;
    }

    /**
     * @return ReadableCollection<int, Image>
     */
    public function getImages(): ReadableCollection
    {
        return $this->images;
    }

    /**
     * @param Collection<int, Image> $images
     */
    public function setImages(Collection $images): void
    {
        $this->images = $images;
    }
}
