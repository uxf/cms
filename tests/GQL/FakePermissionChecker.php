<?php

declare(strict_types=1);

namespace UXF\CMSTests\GQL;

use UXF\Core\Contract\Permission\PermissionChecker;

class FakePermissionChecker implements PermissionChecker
{
    public function isAllowed(string $resourceType, string $resourceName): bool
    {
        return true;
    }
}
