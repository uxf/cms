<?php

declare(strict_types=1);

namespace UXF\CMSTests\GQL;

use Symfony\Component\HttpKernel\KernelInterface;
use UXF\CMSTests\Story\StoryTestCase;
use UXF\Core\Test\Client;

class SmokeGQLTest extends StoryTestCase
{
    private Client $client; // @phpstan-ignore-line

    public function testAutocomplete(): void
    {
        $this->client = self::createClient();

        $response = $this->gql(__DIR__ . '/AutocompleteQuery.graphql', [
            'name' => 'user',
            'term' => 'ROOT',
        ]);
        self::assertSame([
            'data' => [
                'autocomplete' => [
                    [
                        'id' => '1',
                        'label' => 'Root root@uxf.cz',
                    ],
                ],
            ],
        ], $response);
    }

    public function testUserConfig(): void
    {
        $this->client = self::createClient();
        $this->client->login();

        $response = $this->gql(__DIR__ . '/UserConfigMutation.graphql', [
            'name' => 'test',
            'data' => [
                'hello' => 'world',
            ],
        ]);
        self::assertSame([
            'data' => [
                'userConfig' => [
                    'hello' => 'world',
                ],
            ],
        ], $response);

        $this->gql(__DIR__ . '/UserConfigMutation.graphql', [
            'name' => 'test',
            'data' => [
                'wow' => 'ok',
            ],
        ]);

        $response = $this->gql(__DIR__ . '/UserConfigQuery.graphql', [
            'name' => 'test',
        ]);
        self::assertSame([
            'data' => [
                'userConfig' => [
                    'wow' => 'ok',
                ],
            ],
        ], $response);

        $response = $this->gql(__DIR__ . '/UserConfigQuery.graphql', [
            'name' => 'test-non-exists',
        ]);
        self::assertSame([
            'data' => [
                'userConfig' => null,
            ],
        ], $response);
    }

    /**
     * @param array<mixed> $options
     */
    protected static function createKernel(array $options = []): KernelInterface
    {
        return new KernelGQL('test', true);
    }

    /**
     * @param mixed[] $variables
     */
    private function gql(string $query, array $variables): mixed
    {
        $this->client->post('/graphql', [
            'query' => file_get_contents($query),
            'variables' => $variables,
        ]);
        self::assertResponseIsSuccessful();

        return $this->client->getResponseData();
    }
}
