<?php

declare(strict_types=1);

namespace UXF\CMSTests\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use UXF\CMS\Entity\Invitation\PasswordRecovery;
use UXF\CMS\Entity\Role;
use UXF\CMS\Entity\User;
use UXF\Core\Type\Email;

class UserAndRoleDataFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $roleRoot = new Role();
        $roleRoot
            ->setName(Role::ROLE_ROOT)
            ->setTitle('Root');
        $manager->persist($roleRoot);

        $roleAdmin = new Role();
        $roleAdmin
            ->setName(Role::ROLE_ADMIN)
            ->setTitle('Admin');
        $manager->persist($roleAdmin);

        // password: root
        $userRoot = new User(Email::of('root@uxf.cz'), 'Root', '$2y$04$AHUyNJx5yaV/rqPpWU21MO8udEtPadHkvLJB71njPiFVMwNgshGvi', [$roleRoot]);
        $manager->persist($userRoot);

        // password: admin
        $userAdmin = new User(Email::of('admin@uxf.cz'), 'Admin', '$2y$04$LdszprRP8sxPII5/DWaZ..3tn1PMJh2QoXqKN633q/3YT8/GAx82O', [$roleAdmin]);
        $manager->persist($userAdmin);

        // password: old-password
        $passwordRecoveryUser = new User(Email::of('password-recovery@uxf.cz'), 'Recovery password test', '$2y$04$0KNgEKqWZZsb7mMRGt0JYOolxcnaG/TSARYi/ltO8r9iu9Oo5/ddC', [$roleAdmin]);
        $manager->persist($passwordRecoveryUser);

        // emails
        $userInvitation = new PasswordRecovery(Email::of('test@example.com'), 'a1db7dd0bbde9623c97da3c02be31966cf9cc2d9');
        $manager->persist($userInvitation);

        $passwordRecovery = new PasswordRecovery(Email::of('password-recovery@uxf.cz'), 'valid-token');
        $manager->persist($passwordRecovery);

        $manager->flush();
    }
}
