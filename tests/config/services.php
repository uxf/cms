<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\CMS\DataGrid\AnnotationDataGridBuilderFactory;
use UXF\CMS\Entity\User;
use UXF\CMS\Form\AnnotationFormBuilderFactory;
use UXF\CMS\Service\UserService;
use UXF\Core\Test\Client;
use UXF\DataGrid\DataGridFactory;
use UXF\Form\FormFactory;
use UXF\Security\Service\Authenticator;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->public()
        ->autoconfigure()
        ->autowire();

    $services->alias('test.client', Client::class);

    $services->set(Client::class);

    $services->load('UXF\CMSTests\DataFixtures\\', __DIR__ . '/../DataFixtures');

    $services->load('UXF\CMSTests\InternalDataFixtures\\', __DIR__ . '/../InternalDataFixtures');

    $services->load('UXF\CMSTests\Project\\', __DIR__ . '/../Project');

    $services->set(DataGridFactory::class)
        ->arg('$schemaWithHiddenColumns', param('uxf_data_grid.gen.schema_with_hidden_columns'))
        ->arg('$annotationDataGridBuilderFactory', service(AnnotationDataGridBuilderFactory::class));

    $services->set(FormFactory::class)
        ->arg('$annotationFormBuilderFactory', service(AnnotationFormBuilderFactory::class));

    $services->set(UserService::class)->public(); // kernel test

    $parameters = $containerConfigurator->parameters();

    $containerConfigurator->extension('framework', [
        'secret' => 'test-secret',
        'test' => true,
        'validation' => [
            'email_validation_mode' => 'html5',
        ],
        'mailer' => [
            'dsn' => 'null://null',
        ],
        'http_method_override' => false,
    ]);

    $containerConfigurator->extension('security', [
        'password_hashers' => [
            User::class => 'auto',
        ],
        'providers' => [
            'provider' => [
                'entity' => [
                    'class' => User::class,
                    'property' => 'id',
                ],
            ],
        ],
        'firewalls' => [
            'secured' => [
                'provider' => 'provider',
                'stateless' => true,
                'pattern' => '/',
                'custom_authenticators' => [Authenticator::class],
            ],
        ],
        'access_control' => [
            [
                'path' => '^/api/authorize',
                'roles' => 'PUBLIC_ACCESS',
            ],
            [
                'path' => '^/api/cms',
                'roles' => 'ROLE_ROOT',
            ],
        ],
        'role_hierarchy' => [
            'ROLE_ROOT' => ['ROLE_ADMIN'],
        ],
    ]);

    $parameters->set('env(TEST_DATABASE_URL)', __DIR__ . '/../../var/db.sqlite');

    $containerConfigurator->extension('doctrine', [
        'dbal' => [
            'url' => null,
            'driver' => 'pdo_sqlite',
            'path' => '%env(TEST_DATABASE_URL)%',
        ],
        'orm' => [
            'naming_strategy' => 'doctrine.orm.naming_strategy.underscore_number_aware',
            'auto_mapping' => true,
            'enable_lazy_ghost_objects' => true,
            'report_fields_where_declared' => true,
            'mappings' => [
                'test' => [
                    'type' => 'attribute',
                    'dir' => __DIR__ . '/../Entity',
                    'prefix' => 'UXF\CMSTests\Entity',
                ],
            ],
        ],
    ]);

    $containerConfigurator->extension('twig', [
        'strict_variables' => true,
    ]);

    $containerConfigurator->extension('uxf_security', [
        'user_class' => User::class,
        'base_url' => 'https://uxf.cz',
        'public_key' => '%env(AUTH_PUBLIC_KEY)%',
        'private_key' => '%env(AUTH_PRIVATE_KEY)%',
        // frozen time
        'access_token_lifetime' => 'P10Y',
    ]);

    $containerConfigurator->extension('uxf_storage', [
        'filesystems' => [
            'default' => 'local://default/%kernel.project_dir%/tests/public/upload',
        ],
    ]);

    $containerConfigurator->extension('uxf_cms', [
        'audit_token' => 'wow',
        'mailing' => [
            'email_from' => 'info@uxf.cz',
            'email_name' => 'UXF info',
            'invitation_url' => 'https://uxf.cz/renew-password/',
            'recovery_url' => 'https://uxf.cz/renew-password/',
            'cms_recovery_url' => 'https://uxf.cz/admin/cms/renew-password/',
            'email_previews' => '%kernel.project_dir%/tests/Email',
        ],
        'cron' => [
            'sources' => [__DIR__ . '/../Project'],
            'destinations' => [__DIR__ . '/../generated/cron'],
            'stage' => 'test',
        ],
        'logger' => [
            'stdout' => [
                'enabled' => true,
            ],
        ],
        'storage' => [
            'default_upload_namespace' => 'cms',
        ],
        'loggable' => [
            'sources' => '%kernel.project_dir%/tests/Entity',
        ],
    ]);
};
