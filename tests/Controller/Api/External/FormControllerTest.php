<?php

declare(strict_types=1);

namespace UXF\CMSTests\Controller\Api\External;

use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use UXF\CMSTests\Story\StoryTestCase;
use UXF\Storage\Entity\Image;

class FormControllerTest extends StoryTestCase
{
    public function test(): void
    {
        $client = static::createClient();
        $client->login();

        $client->get('/api/cms/form/test-article/schema');
        self::assertResponseIsSuccessful();
        self::assertSame([
            'fields' => [
                [
                    'name' => 'integer',
                    'type' => 'integer',
                    'label' => 'INTEGER',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'float',
                    'type' => 'float',
                    'label' => 'FLOAT',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'string',
                    'type' => 'string',
                    'label' => 'STRING',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'text',
                    'type' => 'text',
                    'label' => 'TEXT',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'date',
                    'type' => 'date',
                    'label' => 'DATE',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'datetime',
                    'type' => 'datetime',
                    'label' => 'DATETIME',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'time',
                    'type' => 'time',
                    'label' => 'TIME',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'phone',
                    'type' => 'phone',
                    'label' => 'PHONE',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'manyToOne',
                    'type' => 'manyToOne',
                    'label' => 'MANY TO ONE',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => 'tag',
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'manyToMany',
                    'type' => 'manyToMany',
                    'label' => 'MANY TO MANY',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => 'tag',
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'oneToMany',
                    'type' => 'oneToMany',
                    'label' => 'ONE TO MANY',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [
                        [
                            'name' => 'text',
                            'type' => 'string',
                            'label' => 'TEXT',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                    ],
                ],
                [
                    'name' => 'oneToOne',
                    'type' => 'oneToOne',
                    'label' => 'ARTICLE UNIQ INFO',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => 'tag',
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'oneToOneFieldset',
                    'type' => 'oneToOneFieldset',
                    'label' => 'ARTICLE FIELDSET INFO',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [
                        [
                            'name' => 'integer',
                            'type' => 'integer',
                            'label' => 'INTEGER?',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'string',
                            'type' => 'string',
                            'label' => 'STRING?',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'text',
                            'type' => 'text',
                            'label' => 'TEXT?',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'date',
                            'type' => 'date',
                            'label' => 'DATE?',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'datetime',
                            'type' => 'datetime',
                            'label' => 'DATETIME?',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'time',
                            'type' => 'time',
                            'label' => 'TIME?',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'manyToOne',
                            'type' => 'manyToOne',
                            'label' => 'MANY TO ONE?',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => 'tag',
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'manyToMany',
                            'type' => 'manyToMany',
                            'label' => 'MANY TO MANY?',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => 'tag',
                            'options' => null,
                            'fields' => [],
                        ],
                    ],
                ],
                [
                    'name' => 'info',
                    'type' => 'embedded',
                    'label' => 'INFO',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [
                        [
                            'name' => 'integer',
                            'type' => 'integer',
                            'label' => 'INTEGER!',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'float',
                            'type' => 'float',
                            'label' => 'FLOAT!',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'string',
                            'type' => 'string',
                            'label' => 'STRING!',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'text',
                            'type' => 'text',
                            'label' => 'TEXT!',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'date',
                            'type' => 'date',
                            'label' => 'DATE!',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'datetime',
                            'type' => 'datetime',
                            'label' => 'DATETIME!',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                        [
                            'name' => 'time',
                            'type' => 'time',
                            'label' => 'TIME!',
                            'required' => true,
                            'readOnly' => false,
                            'editable' => true,
                            'autocomplete' => null,
                            'options' => null,
                            'fields' => [],
                        ],
                    ],
                ],
                [
                    'name' => 'status',
                    'type' => 'enum',
                    'label' => 'STATUS',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => [
                        [
                            'id' => 'NEW',
                            'label' => 'Nový',
                            'color' => 'green',
                        ],
                        [
                            'id' => 'OLD',
                            'label' => 'Starý',
                            'color' => 'red',
                        ],
                    ],
                    'fields' => [],
                ],
                [
                    'name' => 'file',
                    'type' => 'file',
                    'label' => 'FILE',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'image',
                    'type' => 'image',
                    'label' => 'IMAGE',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
                [
                    'name' => 'content',
                    'type' => 'content',
                    'label' => 'CONTENT',
                    'required' => true,
                    'readOnly' => false,
                    'editable' => true,
                    'autocomplete' => null,
                    'options' => null,
                    'fields' => [],
                ],
            ],
        ], $client->getResponseData());

        $data = [
            'integer' => 1,
            'float' => 2.2,
            'string' => '333',
            'text' => '444',
            'date' => '2020-01-01',
            'datetime' => '2020-02-02T10:00:00+01:00',
            'time' => '12:34:56',
            'phone' => '+420774339663',
            'manyToOne' => [
                'id' => 1,
                'label' => 'Tag 1',
            ],
            'manyToMany' => [[
                'id' => 3,
                'label' => 'Tag 3',
            ]],
            'oneToMany' => [[
                'text' => 'WTF?',
            ]],
            'oneToOne' => null,
            'oneToOneFieldset' => [
                'integer' => 9,
                'string' => '8',
                'text' => '7',
                'date' => '2030-01-01',
                'datetime' => '2030-02-02T10:00:00+01:00',
                'time' => '12:34:56',
                'manyToOne' => [
                    'id' => 3,
                    'label' => 'Tag 3',
                ],
                'manyToMany' => [[
                    'id' => 2,
                    'label' => 'Tag 2',
                ]],
            ],
            'info' => [
                'integer' => 1000,
                'float' => 1000,
                'string' => '100',
                'text' => '100!!!',
                'date' => '2040-01-01',
                'datetime' => '2040-02-02T10:00:00+01:00',
                'time' => '12:34:56',
            ],
            'status' => 'NEW',
        ];

        // create
        $client->post('/api/cms/form/test-article', $data);
        self::assertResponseIsSuccessful();

        /** @var mixed $expected */
        $expected = $data;
        $expected += [
            'file' => null,
            'image' => null,
            'content' => [
                'data' => [],
            ],
        ];
        self::assertSame($expected, $client->getResponseData());

        // get created
        $client->get('/api/cms/form/test-article/1');
        self::assertResponseIsSuccessful();
        self::assertSame($expected, $client->getResponseData());

        // create file
        $file = new Image(
            Uuid::fromString('1585a4eb-0f92-4e9b-a1b1-904f09c36930'),
            'jpg',
            'image/jpeg',
            'file.jpg',
            'default',
            123,
            128,
            128,
        );
        /** @var EntityManagerInterface $em */
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $em->persist($file);
        $em->flush();

        $data['oneToOne'] = [
            'id' => 1,
            'label' => 'Uniq top',
        ];
        $data['oneToOneFieldset']['manyToMany'] = [[
            'id' => 1,
            'label' => 'Tag 1',
        ]];
        $data['manyToOne'] = [
            'id' => 3,
            'label' => 'Tag 3',
        ];
        $data['info']['integer'] = 666;
        $data['status'] = 'OLD';
        $data['file'] = 1;
        $data['image'] = 1;
        $data['content'] = [
            'data' => [[
                'type' => 'wysiwyg',
                'property' => 'value',
            ]],
            'search' => 'ok go!',
        ];

        // update
        $client->put('/api/cms/form/test-article/1', $data);
        self::assertResponseIsSuccessful();

        /** @var mixed $expected */
        $expected = $data;
        $expected['file'] = [
            'id' => 1,
            'uuid' => '1585a4eb-0f92-4e9b-a1b1-904f09c36930',
            'type' => 'image/jpeg',
            'extension' => 'jpg',
            'name' => 'file.jpg',
            'namespace' => 'default',
            'width' => 128,
            'height' => 128,
            'size' => 123,
            'createdAt' => '2022-01-01T10:00:00+01:00',
            'fileType' => 'image',
        ];
        $expected['image'] = [
            'id' => 1,
            'uuid' => '1585a4eb-0f92-4e9b-a1b1-904f09c36930',
            'type' => 'image/jpeg',
            'extension' => 'jpg',
            'name' => 'file.jpg',
            'namespace' => 'default',
            'width' => 128,
            'height' => 128,
            'size' => 123,
            'createdAt' => '2022-01-01T10:00:00+01:00',
            'fileType' => 'image',
        ];
        $expected['content'] = [
            'data' => [[
                'type' => 'wysiwyg',
                'property' => 'value',
            ]],
        ];
        self::assertSame($expected, $client->getResponseData());

        // get updated
        $client->get('/api/cms/form/test-article/1');
        self::assertResponseIsSuccessful();
        self::assertSame($expected, $client->getResponseData());
    }
}
