<?php

declare(strict_types=1);

namespace UXF\CMSTests\Controller\Api\External;

use UXF\CMSTests\Story\StoryTestCase;

class DataGridControllerTest extends StoryTestCase
{
    public function test(): void
    {
        $client = static::createClient();
        $client->login();

        $client->get('/api/cms/datagrid/schema/user');
        self::assertResponseIsSuccessful();
        self::assertSame([
            'name' => 'user',
            'columns' => [
                [
                    'name' => 'name',
                    'label' => 'Jméno',
                    'sort' => true,
                    'hidden' => false,
                    'type' => 'string',
                    'config' => [],
                ],
                [
                    'name' => 'email',
                    'label' => 'E-mail',
                    'sort' => true,
                    'hidden' => false,
                    'type' => 'email',
                    'config' => [],
                ],
                [
                    'name' => 'active',
                    'label' => 'Aktivní',
                    'sort' => true,
                    'hidden' => false,
                    'type' => 'boolean',
                    'config' => [],
                ],
                [
                    'name' => 'createdAt',
                    'label' => 'Čas registrace',
                    'sort' => true,
                    'hidden' => false,
                    'type' => 'datetime',
                    'config' => [],
                ],
                [
                    'name' => 'updatedAt',
                    'label' => 'Čas poslední úpravy',
                    'sort' => true,
                    'hidden' => false,
                    'type' => 'datetime',
                    'config' => [],
                ],
                [
                    'name' => 'lastLoginAt',
                    'label' => 'Čas posledního přihlášení',
                    'sort' => true,
                    'hidden' => false,
                    'type' => 'datetime',
                    'config' => [],
                ],
            ],
            'filters' => [
                [
                    'name' => 'name',
                    'label' => 'Jméno',
                    'type' => 'string',
                    'autocomplete' => null,
                    'options' => null,
                ],
                [
                    'name' => 'email',
                    'label' => 'E-mail',
                    'type' => 'string',
                    'autocomplete' => null,
                    'options' => null,
                ],
                [
                    'name' => 'active',
                    'label' => 'Aktivní',
                    'type' => 'select',
                    'autocomplete' => null,
                    'options' => [
                        [
                            'id' => 1,
                            'label' => 'Ano',
                        ],
                        [
                            'id' => 0,
                            'label' => 'Ne',
                        ],
                    ],
                ],
                [
                    'name' => 'createdAt',
                    'label' => 'Čas registrace',
                    'type' => 'datetime',
                    'autocomplete' => null,
                    'options' => null,
                ],
                [
                    'name' => 'updatedAt',
                    'label' => 'Čas poslední úpravy',
                    'type' => 'datetime',
                    'autocomplete' => null,
                    'options' => null,
                ],
                [
                    'name' => 'lastLoginAt',
                    'label' => 'Čas posledního přihlášení',
                    'type' => 'datetime',
                    'autocomplete' => null,
                    'options' => null,
                ],
            ],
            'tabs' => [],
            'fullText' => false,
            's' => [
                'name' => 'id',
                'dir' => 'asc',
            ],
            'perPage' => 10,
            'sort' => 'id',
            'dir' => 'asc',
        ], $client->getResponseData());
    }

    public function testEnum(): void
    {
        $client = static::createClient();
        $client->login();

        $client->get('/api/cms/datagrid/schema/test-article');
        self::assertResponseIsSuccessful();
        self::assertSame([
            'name' => 'test-article',
            'columns' => [
                [
                    'name' => 'status',
                    'label' => 'status',
                    'sort' => true,
                    'hidden' => false,
                    'type' => 'string',
                    'config' => [],
                ],
            ],
            'filters' => [
                [
                    'name' => 'status',
                    'label' => 'status',
                    'type' => 'select',
                    'autocomplete' => null,
                    'options' => [
                        [
                            'id' => 'NEW',
                            'label' => 'Nový',
                        ],
                        [
                            'id' => 'OLD',
                            'label' => 'Starý',
                        ],
                    ],
                ],
            ],
            'tabs' => [],
            'fullText' => false,
            's' => [
                'name' => 'id',
                'dir' => 'asc',
            ],
            'perPage' => 10,
            'sort' => 'id',
            'dir' => 'asc',
        ], $client->getResponseData());
    }
}
