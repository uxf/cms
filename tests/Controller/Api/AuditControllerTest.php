<?php

declare(strict_types=1);

namespace UXF\CMSTests\Controller\Api;

use UXF\CMSTests\Story\StoryTestCase;

class AuditControllerTest extends StoryTestCase
{
    public function test(): void
    {
        $client = self::createClient();
        $client->get('/api/_cms-audit?token=wow');
        self::assertResponseIsSuccessful();
        self::assertArrayHasKey('php', $client->getResponseData());
    }
}
