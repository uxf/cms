<?php

declare(strict_types=1);

namespace UXF\CMSTests\Controller\Api;

use Nette\Utils\Json;
use Nette\Utils\JsonException;
use UXF\CMSTests\Story\StoryTestCase;
use UXF\Core\Test\Client;

class PasswordRecoveryControllerTest extends StoryTestCase
{
    private Client $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
    }

    /**
     * @throws JsonException
     */
    public function testForgottenPassword(): void
    {
        $this->client->request(
            'POST',
            '/api/forgotten-password',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/javascript',
            ],
            Json::encode([
                'email' => 'admin@uxf.cz',
            ]),
        );
        self::assertResponseIsSuccessful();
        self::assertEquals('{}', $this->client->getResponse()->getContent());
    }

    /**
     * @throws JsonException
     */
    public function testPasswordRecoveryTokenNotFound(): void
    {
        $this->client->request(
            'POST',
            '/api/password-recovery',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/javascript',
            ],
            Json::encode([
                'password' => 'new-password',
                'token' => 'wrong-token',
            ]),
        );

        self::assertResponseStatusCodeSame(400);
    }

    /**
     * @throws JsonException
     */
    public function testPasswordRecoveryValidToken(): void
    {
        // login with old password
        $this->client->request(
            'POST',
            '/api/auth/login',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            Json::encode([
                'username' => 'password-recovery@uxf.cz',
                'password' => 'old-password',
            ]),
        );

        self::assertResponseIsSuccessful();

        // forgotten password
        $this->client->request(
            'POST',
            '/api/password-recovery',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            Json::encode([
                'password' => 'new-password',
                'token' => 'valid-token',
            ]),
        );

        self::assertResponseIsSuccessful();
        self::assertEquals('{}', $this->client->getResponse()->getContent());

        // login with new password
        $this->client->request(
            'POST',
            '/api/auth/login',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            Json::encode([
                'username' => 'password-recovery@uxf.cz',
                'password' => 'new-password',
            ]),
        );

        self::assertResponseIsSuccessful();
    }
}
