<?php

declare(strict_types=1);

namespace UXF\CMSTests\Controller\Api;

use Symfony\Component\HttpFoundation\Response;
use UXF\CMSTests\Story\StoryTestCase;
use UXF\Core\Test\Client;

class LoginControllerTest extends StoryTestCase
{
    private Client $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
    }

    public function testLoginAndLogoutWithSecuredArea(): void
    {
        $this->client->get('/api/cms/tables');
        self::assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);

        $this->client->login();
        self::assertResponseStatusCodeSame(Response::HTTP_OK);

        $this->client->get('/api/cms/tables');
        self::assertResponseStatusCodeSame(Response::HTTP_OK);

        $this->client->logout();
        self::assertResponseStatusCodeSame(Response::HTTP_OK);

        $this->client->get('/api/cms/tables');
        self::assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);
    }
}
