<?php

declare(strict_types=1);

namespace UXF\CMSTests\Controller\Api\Cms;

use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Symfony\Component\HttpFoundation\Response;
use UXF\CMSTests\Story\StoryTestCase;
use UXF\Core\Test\Client;

class CmsTableRowsControllerTest extends StoryTestCase
{
    public function testTableRows(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/cms/datagrid/user');
        self::assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);

        $client->login();

        $client->request('GET', '/api/cms/datagrid/user', [
            'f' => [[
                'name' => 'name',
                'value' => 'root',
            ]],
        ]);
        self::assertResponseIsSuccessful();

        $data = Json::decode((string) $client->getResponse()->getContent(), forceArrays: true);

        self::assertArrayHasKey('result', $data);
        self::assertArrayHasKey('count', $data);
        self::assertArrayHasKey('totalCount', $data);
    }

    public function testTableRowsCRUD(): void
    {
        $client = self::createClient();
        $this->crudTestTableRowsGet($client);
        $id = $this->crudTestTableRowsPost($client);
        $this->crudTestTableRowsPut($client, $id);
        $this->crudTestTableRowsDelete($client, $id);
    }

    private function crudTestTableRowsGet(Client $client): void
    {
        $client->request('GET', '/api/cms/form/user/1');
        self::assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);

        $client->request('POST', '/api/cms/form/user');
        self::assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);

        $client->login();

        $client->request('GET', '/api/cms/form/user/1');
        self::assertResponseIsSuccessful();

        $data = Json::decode((string) $client->getResponse()->getContent(), forceArrays: true);
        self::assertArrayHasKey('id', $data, 'table rowID - has ID');
        self::assertArrayHasKey('name', $data, 'table rowID - has name');
        self::assertArrayHasKey('email', $data, 'table rowID - has email');
        self::assertArrayHasKey('active', $data, 'table rowID - has active');
        self::assertArrayHasKey('createdAt', $data, 'table rowID - has createdAt');
        self::assertArrayHasKey('updatedAt', $data, 'table rowID - has updatedAt');

        $client->request('GET', '/api/cms/form/user/999999');
        self::assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    /**
     * @throws JsonException
     */
    private function crudTestTableRowsPost(Client $client): int
    {
        $client->logout();
        $client->request('POST', '/api/cms/form/user');
        self::assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);

        $client->login();

        $client->request(
            'POST',
            '/api/cms/form/user',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            Json::encode([
                'name' => 'Admin3',
                'email' => 'admin3@uxf.cz',
                'active' => true,
            ]),
        );
        self::assertResponseIsSuccessful();

        $data = Json::decode((string) $client->getResponse()->getContent(), forceArrays: true);
        self::assertArrayHasKey('id', $data, 'table rowID - has ID');
        self::assertArrayHasKey('name', $data, 'table rowID - has name');
        self::assertArrayHasKey('email', $data, 'table rowID - has email');
        self::assertArrayHasKey('active', $data, 'table rowID - has active');
        self::assertArrayHasKey('createdAt', $data, 'table rowID - has createdAt');
        self::assertArrayHasKey('updatedAt', $data, 'table rowID - has updatedAt');

        self::assertEquals('Admin3', $data['name'], 'table rowID - created name value');

        return $data['id'];
    }

    private function crudTestTableRowsPut(Client $client, int $id): void
    {
        $client->logout();
        $client->request('PUT', "/api/cms/form/user/$id");
        self::assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);

        $client->login();

        $client->request(
            'PUT',
            "/api/cms/form/user/$id",
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            Json::encode([
                'name' => 'Admin33',
            ]),
        );
        self::assertResponseIsSuccessful();

        $data = Json::decode((string) $client->getResponse()->getContent(), forceArrays: true);
        self::assertArrayHasKey('id', $data, 'table rowID - has ID');
        self::assertArrayHasKey('name', $data, 'table rowID - has name');
        self::assertArrayHasKey('email', $data, 'table rowID - has email');
        self::assertArrayHasKey('active', $data, 'table rowID - has active');
        self::assertArrayHasKey('createdAt', $data, 'table rowID - has createdAt');
        self::assertArrayHasKey('updatedAt', $data, 'table rowID - has updatedAt');

        self::assertEquals('Admin33', $data['name'], 'table rowID - is updated name equal');
    }

    private function crudTestTableRowsDelete(Client $client, int $id): void
    {
        $client->logout();
        $client->request('DELETE', "/api/cms/form/user/$id");
        self::assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);

        $client->login();

        $client->request('DELETE', "/api/cms/form/user/$id");
        self::assertResponseIsSuccessful();
    }

    public function testTableRowsCsv(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/cms/datagrid/export/user');
        self::assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);

        $client->login();

        $client->request('GET', '/api/cms/datagrid/export/user');
        self::assertResponseIsSuccessful();
    }

    /**
     * @throws JsonException
     */
    public function testTableRowsTables(): void
    {
        $entityAliases = [
            'role',
            'user',
            'user-invitation',
        ];

        $client = self::createClient();
        $client->login();

        foreach ($entityAliases as $entityAlias) {
            $client->request('GET', "/api/cms/datagrid/$entityAlias");
            self::assertResponseIsSuccessful();

            $data = Json::decode((string) $client->getResponse()->getContent(), forceArrays: true);

            self::assertArrayHasKey('result', $data);
            self::assertArrayHasKey('count', $data);
            self::assertArrayHasKey('totalCount', $data);
        }
    }

    public function testTableRowsCsvTables(): void
    {
        $entityAliases = [
            'user',
            'role',
            'user-invitation',
        ];

        $client = self::createClient();
        $client->login();

        foreach ($entityAliases as $entityAlias) {
            $client->request('GET', "/api/cms/datagrid/export/$entityAlias");
            self::assertResponseIsSuccessful();
        }
    }

    /**
     * @throws JsonException
     */
    public function testAutocomplete(): void
    {
        $client = self::createClient();
        $client->login();

        $client->request('GET', '/api/cms/form/_autocomplete/role?term=ADM');
        self::assertResponseIsSuccessful();

        $data = Json::decode((string) $client->getResponse()->getContent(), forceArrays: true);
        self::assertEquals('Admin', $data[0]['label']);
    }

    public function testFormValues(): void
    {
        $entityAliases = ['user'];

        $client = self::createClient();
        $client->login();

        foreach ($entityAliases as $entityAlias) {
            $client->request(
                'GET',
                "/api/cms/form/$entityAlias/1",
            );
            self::assertResponseIsSuccessful();
        }
    }
}
