<?php

declare(strict_types=1);

namespace UXF\CMSTests\Controller\Api\Cms;

use Symfony\Component\HttpFoundation\Response;
use UXF\CMSTests\Story\StoryTestCase;

class UserControllerTest extends StoryTestCase
{
    public function testGetFailed(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/cms/user');
        self::assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);

        $client->login();

        $client->request('POST', '/api/cms/user');
        self::assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    public function testGetSuccess(): void
    {
        $client = self::createClient();
        $client->login();

        $client->request('GET', '/api/cms/user');
        self::assertResponseIsSuccessful();
        $data = $client->getResponseData();
        self::assertArrayHasKey('user', $data, 'response has key user');
        self::assertArrayHasKey('userConfig', $data, 'response has key userConfig');
    }
}
