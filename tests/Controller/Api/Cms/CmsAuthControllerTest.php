<?php

declare(strict_types=1);

namespace UXF\CMSTests\Controller\Api\Cms;

use Symfony\Component\HttpFoundation\Response;
use UXF\CMS\Entity\Role;
use UXF\CMS\Entity\User;
use UXF\CMS\Repository\UserRepository;
use UXF\CMSTests\Story\StoryTestCase;

class CmsAuthControllerTest extends StoryTestCase
{
    public function testLogin(): void
    {
        $client = self::createClient();

        $client->request('GET', '/api/auth/login');
        self::assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);

        $client->request('POST', '/api/auth/login');
        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);

        $client->post('/api/auth/login', [
            'username' => 'root@uxf.cz',
            'password' => 'wrongpassword',
        ]);
        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);

        $client->post('/api/auth/login', [
            'username' => 'root@uxf.cz',
            'password' => 'root',
        ]);
        self::assertResponseIsSuccessful();

        $data = $client->getResponseData();

        self::assertArrayHasKey('accessToken', $data, 'login response - has access_token');
        self::assertArrayHasKey('refreshToken', $data, 'login response - has refresh_token');
    }

    public function testInviteAndCreateUserFail(): void
    {
        $client = self::createClient();
        $client->login();

        $client->post('/api/cms/invite-user', [
            'email' => '',
            'name' => 'Test user',
            'roles' => [],
        ]);
        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);

        $data = $client->getResponseData();
        self::assertSame([
            'code' => 'BAD_USER_INPUT',
            'message' => 'INVALID INPUT',
        ], $data['error']);
        self::assertSame([
            [
                'field' => 'email',
                'message' => 'Invalid email format [Supported format is joe@black.com => \'\' value given]',
            ],
        ], $data['validationErrors']);
    }

    public function testInviteAndCreateUser(): void
    {
        $client = self::createClient();
        $client->login();

        $client->post('/api/cms/invite-user', [
            'name' => 'Test user',
            'email' => 'invite-user@example.com',
            'roles' => [1, 2],
        ]);
        self::assertResponseIsSuccessful();

        /** @var UserRepository $userRepository */
        $userRepository = self::getContainer()->get(UserRepository::class);

        /** @var User $user */
        $user = $userRepository->findByUsername('invite-user@example.com');
        self::assertSame('Test user', $user->getName());
        self::assertSame([Role::ROLE_ROOT, Role::ROLE_ADMIN], $user->getRoles());
    }
}
