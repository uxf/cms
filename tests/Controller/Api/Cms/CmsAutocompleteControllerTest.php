<?php

declare(strict_types=1);

namespace UXF\CMSTests\Controller\Api\Cms;

use UXF\CMSTests\Story\StoryTestCase;

class CmsAutocompleteControllerTest extends StoryTestCase
{
    public function test(): void
    {
        $client = static::createClient();
        $client->login();

        $client->get('/api/cms/autocomplete/user?' . http_build_query([
            'term' => 'ÁdMIN',
        ]));
        self::assertResponseIsSuccessful();
        self::assertSame([
            [
                'id' => 2,
                'label' => 'Admin admin@uxf.cz',
            ],
        ], $client->getResponseData());

        $client->get('/api/cms/autocomplete/role?' . http_build_query([
            'term' => 'rÓÓť',
        ]));
        self::assertResponseIsSuccessful();
        self::assertSame([
            [
                'id' => 1,
                'label' => 'Root',
            ],
        ], $client->getResponseData());

        $client->get('/api/cms/autocomplete/role?limit=1');
        self::assertResponseIsSuccessful();
        self::assertSame([
            [
                'id' => 2,
                'label' => 'Admin',
            ],
        ], $client->getResponseData());
    }
}
