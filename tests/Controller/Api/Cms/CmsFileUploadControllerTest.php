<?php

declare(strict_types=1);

namespace UXF\CMSTests\Controller\Api\Cms;

use Nette\Utils\Json;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use UXF\CMSTests\Story\StoryTestCase;

use function Safe\copy;

class CmsFileUploadControllerTest extends StoryTestCase
{
    private UploadedFile $uploadedFile;

    public function setUp(): void
    {
        parent::setUp();

        // create file copy for upload testing
        $uploadedFile = __DIR__ . '/../../../files/file.png';
        $tmpFile = '/tmp/tmp_file.png';
        copy($uploadedFile, $tmpFile);

        $this->uploadedFile = new UploadedFile(
            $tmpFile,
            'file.png',
            'image/png',
            UPLOAD_ERR_OK,
        );
    }

    public function testUploadCms(): void
    {
        $client = self::createClient();

        $client->request('POST', '/api/cms/files/upload');
        self::assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);

        $client->login();

        $client->request('GET', '/api/cms/files/upload');
        self::assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED);

        $client->request(
            'POST',
            '/api/cms/files/upload',
            [],
            [
                'files' => [$this->uploadedFile],
            ],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
        );

        self::assertResponseIsSuccessful();
        self::assertCount(1, $client->getRequest()->files->all());

        $data = Json::decode((string) $client->getResponse()->getContent(), forceArrays: true)[0];

        self::assertSame(1, $data['id']);
        self::assertSame('image/png', $data['type']);
        self::assertSame('png', $data['extension']);
        self::assertSame('file.png', $data['name']);
        self::assertSame('cms', $data['namespace']);
    }
}
