<?php

declare(strict_types=1);

namespace UXF\CMSTests\Controller\Api\Cms;

use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use UXF\CMSTests\Story\StoryTestCase;
use UXF\Core\Test\Client;

class CmsChangePasswordControllerTest extends StoryTestCase
{
    private Client $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->client->login();
    }

    /**
     * @throws JsonException
     */
    public function testChangePasswordFailNoData(): void
    {
        $this->client->request(
            'POST',
            '/api/cms/change-password',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
        );
        self::assertResponseStatusCodeSame(JsonResponse::HTTP_BAD_REQUEST);
    }

    /**
     * @throws JsonException
     */
    public function testChangePasswordFail(): void
    {
        $this->client->request(
            'POST',
            '/api/cms/change-password',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            Json::encode([
                'originalPassword' => 'xxx',
                'newPassword' => 'xxx',
                'newPasswordAgain' => 'xxx',
            ]),
        );
        self::assertResponseIsSuccessful();

        $data = Json::decode((string) $this->client->getResponse()->getContent(), forceArrays: true);

        self::assertArrayHasKey('success', $data);
        self::assertFalse($data['success']);
    }

    /**
     * @throws JsonException
     */
    public function testChangePasswordOk(): void
    {
        $this->client->request(
            'POST',
            '/api/cms/change-password',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            Json::encode([
                'originalPassword' => 'root',
                'newPassword' => 'root',
                'newPasswordAgain' => 'root',
            ]),
        );
        self::assertResponseIsSuccessful();

        $data = Json::decode((string) $this->client->getResponse()->getContent(), forceArrays: true);

        self::assertArrayHasKey('success', $data);
        self::assertTrue($data['success']);
    }
}
