<?php

declare(strict_types=1);

namespace UXF\CMSTests\Controller\Api\Cms;

use UXF\CMSTests\Story\StoryTestCase;
use UXF\Core\Test\Client;

class CmsTablesControllerTest extends StoryTestCase
{
    private Client $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->client->login();
    }

    public function testTables(): void
    {
        $this->client->request(
            'GET',
            '/api/cms/tables',
        );
        self::assertResponseIsSuccessful();
    }
}
