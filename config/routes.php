<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\Security\Controller\LoginController;
use UXF\Security\Controller\LogoutController;

return static function (RoutingConfigurator $routes): void {
    $routes->import(__DIR__ . '/../src/Controller/');

    // BC
    $routes->add('api_login', '/api/login')
        ->controller(LoginController::class)
        ->methods(['POST']);

    $routes->add('api_logout', '/api/logout')
        ->controller(LogoutController::class)
        ->methods(['GET']);
};
