<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\CMS\Autocomplete\DefaultAutocompleteFactory;
use UXF\CMS\Command\CronJobGeneratorCommand;
use UXF\CMS\Command\LintAnnotationsCommand;
use UXF\CMS\DataGrid\AnnotationDataGridBuilderFactory;
use UXF\CMS\EventSubscriber\CronJobEventSubscriber;
use UXF\CMS\Form\AnnotationFormBuilderFactory;
use UXF\CMS\Form\Mapper\ContentMapper;
use UXF\CMS\Form\Mapper\FileCollectionMapper;
use UXF\CMS\Form\Mapper\FileMapper;
use UXF\CMS\Form\Mapper\ImageCollectionMapper;
use UXF\CMS\Form\Mapper\ImageMapper;
use UXF\CMS\GQL\AutocompleteResponseProvider;
use UXF\CMS\Repository\UserRepository;
use UXF\CMS\Service\EntityLoader;
use UXF\Core\Contract\Autocomplete\AutocompleteFactory;
use UXF\Core\Contract\Metadata\MetadataLoader;
use UXF\DataGrid\DataGridFactory;
use UXF\Form\FormFactory;
use UXF\Security\Service\AuthUserProvider;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure()
        ->bind('$projectDir', '%kernel.project_dir%')
        ->bind('$debug', '%kernel.debug%');

    $services->load('UXF\CMS\Controller\\', __DIR__ . '/../src/Controller')
        ->public();

    $services->load('UXF\CMS\Command\\', __DIR__ . '/../src/Command');
    $services->load('UXF\CMS\Content\\', __DIR__ . '/../src/Content');
    $services->load('UXF\CMS\Security\\', __DIR__ . '/../src/Security');
    $services->load('UXF\CMS\Repository\\', __DIR__ . '/../src/Repository');
    $services->load('UXF\CMS\Service\\', __DIR__ . '/../src/Service');
    $services->load('UXF\CMS\Utils\\', __DIR__ . '/../src/Utils');

    $services->set(MetadataLoader::class, EntityLoader::class);

    $services->set(LintAnnotationsCommand::class)
        ->arg('$container', service('service_container'));

    // autocomplete
    $services->load('UXF\CMS\Autocomplete\\', __DIR__ . '/../src/Autocomplete');
    $services->set(AutocompleteFactory::class, DefaultAutocompleteFactory::class);

    $services->load('UXF\CMS\DataGrid\\', __DIR__ . '/../src/DataGrid');

    $services->set(DataGridFactory::class)
        ->arg('$schemaWithHiddenColumns', param('uxf_data_grid.gen.schema_with_hidden_columns'))
        ->arg('$annotationDataGridBuilderFactory', service(AnnotationDataGridBuilderFactory::class));

    $services->load('UXF\CMS\Form\\', __DIR__ . '/../src/Form');

    $services->set(FormFactory::class)
        ->arg('$annotationFormBuilderFactory', service(AnnotationFormBuilderFactory::class));

    // cron
    $services->set(CronJobGeneratorCommand::class)
        ->arg('$sources', param('uxf_cms.cron.sources'))
        ->arg('$destinations', param('uxf_cms.cron.destinations'));

    $services->set(CronJobEventSubscriber::class)
        ->arg('$appStage', param('uxf_cms.cron.stage'))
        ->arg('$enabledStages', param('uxf_cms.cron.enabled_stages'));

    // security
    $services->alias(AuthUserProvider::class, UserRepository::class);

    // gql
    $services->set(AutocompleteResponseProvider::class);

    // form mappers
    $services->set(ContentMapper::SERVICE_ID, ContentMapper::class)->public();
    $services->set(FileMapper::SERVICE_ID, FileMapper::class)->public();
    $services->set(FileCollectionMapper::SERVICE_ID, FileCollectionMapper::class)->public();
    $services->set(ImageMapper::SERVICE_ID, ImageMapper::class)->public();
    $services->set(ImageCollectionMapper::SERVICE_ID, ImageCollectionMapper::class)->public();
};
